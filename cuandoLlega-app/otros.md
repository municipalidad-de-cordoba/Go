## Ayudas varias

Agregar una nueva versionde gradle
Ver versiones: https://developer.android.com/studio/releases/gradle-plugin

```
gradle wrapper
./gradlew wrapper --gradle-version 5.3.1
export CORDOVA_ANDROID_GRADLE_DISTRIBUTION_URL="https\://services.gradle.org/distributions/gradle-5.3.1-all.zip"

# agrega esta version y la ejecuto desde ahi en el directorio del proyecto

export ANDROID_HOME=/home/USER/Android/Sdk
export PATH=${PATH}:$ANDROID_HOME/tools:$ANDROID_HOME/platform-tools

cd platforms/android
/home/hudson/.gradle/wrapper/dists/gradle-3.4.1-bin/71zneekfcxxu7l9p7nr2sc65s/gradle-3.4.1/bin/gradle wrapper

```
### Minificar el código

[Acá](https://developer.android.com/studio/build/shrink-code?hl=es-419) se explica como minificar el código.
Es necesario agregar líneas al archivo _/platforms/android/app/build.gradle_
```
buildTypes {
            release {
                signingConfig signingConfigs.release
                // AGREGAR ESTAS DOS LINEAS *********************************
                minifyEnabled true
                proguardFiles getDefaultProguardFile('proguard-android.txt'),'proguard-rules.pro'
                // AGREGAR ESTAS DOS LINEAS *********************************
            }

        }
```
Esto puede requerir instalar nuevas herramientas en SDK-tools.  

### Escribir logs del sistema

Para escribir logs del sistema y luego leerlos desde la App:
1. Importar `import { LoggerServiceProvider } from "{{ dir }} providers/logger-service/logger-service";`
1. Añadir al constructor `private logs:LoggerServiceProvider`
1. Hay 6 métodos en este servicio para loguear:
	1. Las que SOLO escriben a consola. (Son lo mismo que console.log, respectivo para cada tipo de mensaje)
		1. `cLog(msg:any)` **Lo mismo que `console.log()`**
		1. `cWarn(msg:any)` **Lo mismo que `console.warn()`**
		1. `cErr(msg:any)` **Lo mismo que `console.err()`**
	1. Las que además de escribir a consola escriben a la App
		1. `dbLog(msg:any)` **Escribe un mensaje de notificacion Y `console.log()`**
		1. `dbWarn(msg:any)` **Escribe un mensaje de advertencia Y `console.warn()`**
		1. `dbErr(msg:any)` **Escribe un mensaje de error Y `console.err()`**
		
