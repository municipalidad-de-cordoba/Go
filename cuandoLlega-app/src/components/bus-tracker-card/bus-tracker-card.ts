// import { MessagesServiceProvider } from './../../providers/messages-service/messages-service';
// import { TrackingServiceProvider } from './../../providers/tracking-service/tracking-service';
// import { TapticEngine } from '@ionic-native/taptic-engine';
// import { Parada, Favorito, Seguimiento } from './../../app/globals';
// import { Component, Input, Output, EventEmitter } from '@angular/core';
// import {Observable} from 'rxjs';

import { Component} from '@angular/core';
@Component({
  selector: 'bus-tracker-card',
  templateUrl: 'bus-tracker-card.html'
})
export class BusTrackerCardComponent  {

  // parada:Parada;
  // // recorrido:Recorrido;
  // tiempo:number;
  // tRestante:number;
  // tRestanteSecs:number;
  // // poste:Poste;
  // initClock:boolean;
  // tickPosition:number;
  // tick:boolean; //Para animacion de los minutos
  //
  // favorito:boolean=false;
  //
  //
  // @Input() seguimiento: Seguimiento;


  // @Output() timePassed:EventEmitter<any> = new EventEmitter();
  // constructor(private taptic:TapticEngine, private trackingService:TrackingServiceProvider,
  // private messageService:MessagesServiceProvider){
  //
  // }

  constructor(){}

  // ngOnInit() {
  //   this.parada = this.seguimiento.parada;
  //   this.tiempo = this.seguimiento.tiempo;
  //   this.tRestante = this.tiempo; //COMO ME VAN A PASAR EL TIEMPO? EN MIN, EN SEGS?
  //   this.tRestanteSecs = this.tRestante*60;
  //   this.initClock = false;
  //   this.tick = false;
  //   let t = this.tRestante - 1;
  //   if(t<19){
  //     this.tickPosition = t+11;
  //   }else if (t>=19 && t<50){
  //     this.tickPosition = t-19;
  //   }else if(t>=50 && t<59){
  //     this.tickPosition = t-49;
  //   }
  //   if(this.trackingService.getFavorito(new Favorito(this.parada))!=null){
  //     this.favorito = true;
  //   }
  //   this.startTimer();
  //  }
  // startTimer(){
  //   setTimeout (() => {
  //     this.initClock=true; //Esperando que se cargue el reloj
  //   }, 2000);
  //   let tAnt = this.tRestanteSecs;
  //   this.seguimiento.timer = Observable.interval(1000).subscribe(x => {
  //     if(this.initClock){
  //       if(this.tRestanteSecs>0){
  //         // console.log(this.tRestanteSecs);
  //
  //         if(this.tRestanteSecs==1){
  //           setTimeout (() => {
  //             this.tRestanteSecs = 0;
  //             this.tRestante = 0;
  //             this.taptic.notification({type: 'success'});
  //             this.timePassed.emit({
  //               parada: this.parada,
  //               tiempo: this.tRestanteSecs
  //             });
  //           }, 1000);
  //         }else{
  //           if(tAnt - this.tRestanteSecs == 60){
  //             this.tRestante -= 1;
  //             tAnt = this.tRestanteSecs;
  //           }
  //           this.tRestanteSecs = this.tRestanteSecs - 1;
  //           this.timePassed.emit({
  //             parada: this.parada,
  //             tiempo: this.tRestanteSecs
  //           });
  //         }
  //
  //       }else{
  //         this.seguimiento.timer.unsubscribe();
  //       }
  //     }
  //   });
  // }
  //
  // arrayClock(n: number): any[] {
  //   return Array(n);
  // }
  //
  // marcarFavorito(){
  //   let auxFavorito = new Favorito(this.parada);
  //   console.log(auxFavorito);
  //   if(this.trackingService.getFavorito(auxFavorito)==null){
  //     this.trackingService.addFavorito(auxFavorito);
  //     this.favorito = true;
  //     this.messageService.presentToast("Esta combinación de parada y línea ha sido añadida a tus favoritos.", "middle", false, 3000);
  //   }else{
  //     this.trackingService.removeFavorito(auxFavorito);
  //     this.favorito = false;
  //     this.messageService.presentToast("Esta combinación de parada y línea se ha eliminado de tus favoritos.", "middle", false, 3000);
  //   }
  //   console.log(this.trackingService.getAllFavorites());
  //   this.taptic.impact({
  //     style: "medium"
  //   });
  // }
  //
  // ngOnDestroy() {
  //   this.seguimiento.timer.unsubscribe();
  // }

}
