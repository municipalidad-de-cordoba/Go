import { Component, Input } from '@angular/core';

/**
 * Generated class for the TroncalListComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'troncal-list',
  templateUrl: 'troncal-list.html'
})
export class TroncalListComponent {
  @Input ('listTroncales') troncales;
  listTroncales : any;
  text: string;

  constructor() {
    console.log('Hello TroncalListComponent Component');
    this.text = 'Hello World';
  }

  ngAfterViewInit(){
    this.listTroncales = this.troncales.reverse();
  }

  getLineas(t){

  }
}
