import { NgModule } from '@angular/core';
import { TroncalListComponent } from './troncal-list/troncal-list';
import { IonicModule } from 'ionic-angular';
import { BusTrackerCardComponent } from './bus-tracker-card/bus-tracker-card';
import { ExpandableComponent } from './expandable/expandable';

@NgModule({
	declarations: [TroncalListComponent,
    BusTrackerCardComponent,
    ExpandableComponent],
	imports: [IonicModule],
	exports: [TroncalListComponent,
    BusTrackerCardComponent,
    ExpandableComponent]
})
export class ComponentsModule {}
