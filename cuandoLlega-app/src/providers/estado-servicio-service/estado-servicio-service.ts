import { Injectable } from '@angular/core';
import { ApiServiceProvider } from "../api-service/api-service";
import { IDatosInternosResponseData } from "../../shared/model/datos-internos-response-data.model";
import { Observable } from "rxjs/Observable";
import "rxjs/add/observable/timer";
import "rxjs/add/operator/mergeMap";
import "rxjs/add/operator/map";

@Injectable()
export class EstadoServicioServiceProvider {

  private _initialDelay = 1500;
  private _halfHourPeriod = 1000 * 60 * 30;

  interval$: Observable<number> = null;

  constructor(private apiService: ApiServiceProvider) {
  }

  private initializeInterval() {
    this.interval$ = Observable.timer(this._initialDelay, this._halfHourPeriod)
      .mergeMap(() => this.apiService.getDatosInternos())
      .map((data: IDatosInternosResponseData[]) => data[0].registrados);
  }

  public getEstadoServicio(): Observable<number> {
    if (this.interval$ == null) {
      this.initializeInterval();
    }

    return this.interval$;
  }
}

