import { Injectable } from '@angular/core';
import { HTTP, HTTPResponse} from '@ionic-native/http/ngx';
import { Platform } from 'ionic-angular';
import { UserMessage } from '../../app/globals';
import { CacheProvider } from '../cache-service/cache-service';
import { CacheType } from '../cache-service/cache-enum';

@Injectable()
export class AuthServiceProvider {

  protected baseURL : string = "https://gobiernoabierto.cordoba.gob.ar/";
  protected apiUrl : string = this.baseURL + "rest-auth/";

  platform:Platform;
  platformStr = "";

  constructor(public http: HTTP, platform: Platform, private cache: CacheProvider) {
    this.platform = platform;
    if (this.platform.is('ios')) this.platformStr = "-ios";
  }

  getVersion(version){
    return version + this.platformStr;
  }

  /**
   * Obtiene los headers de Autenticación necesarios para comunicarse con el API.
   */
  private getAuthHeaders(isGetReq?) {
    return new Promise((resolve) => {
      this.cache.getItemAsync(CacheType.USER_DATA).then((tmpVal) => {
        let userData = JSON.parse(tmpVal);
        if (!isGetReq) return resolve({"Authorization": "Token " + userData.key, "Content-Type": "application/json"});
        else return resolve("Authorization: Token" + userData.key + ", Content-Type: application/json");
      });
    });
  }

  postData(credentials, type): any {
    return new Promise((resolve, reject) => {
      // console.log(this.apiUrl+type+"/");
      // console.log(credentials);
      this.http.setDataSerializer("json");
      this.http.post(this.apiUrl+type+"/", credentials, {}).then(data => {
        resolve(data);
      })
      .catch(error => {
        reject(error);
        console.log(error.status);
        console.log(error.error); // error message as string
        console.log(error.headers);
        console.log(error);
      });
    });
  }

  googleLogin(credentials){
    console.log(credentials);
    return new Promise((resolve, reject) => {
      this.http.setDataSerializer("json");
      console.log(credentials);
      var googleUrl = "https://gobiernoabierto.cordoba.gob.ar/api/v2/login-usuarios/google-users/create_from_google_token/";
      this.http.post(googleUrl, credentials, {}).then(data => {
        resolve(data);
      })
      .catch(error => {
        reject(error);
        console.log(error.status);
        console.log(error.error); // error message as string
        console.log(error.headers);
        console.log(error);
      });
    });
  }

  postRequest(lat, lng, version) {
    version = this.getVersion(version);
    let postParams =  {
      "sistema": "2",  // es el código de la app cuando llega
      "codigo_accion": "INGRESO",
      "detalles": [
                 {"codigo": "latitud", "decimal_val": lat},
                 {"codigo": "longitud", "decimal_val": lng},
                 {"codigo": "Version", "str_val": version}]
       }
    /*const data = JSON.parse(localStorage.getItem("userData"));
    let headers = {"Authorization": "Token "+data.key,
                    "Content-Type": "application/json"};*/
    this.http.setDataSerializer("json");
    this.getAuthHeaders().then((auth) => {
      this.http.post('https://gobiernoabierto.cordoba.gob.ar/api/v2/log-usuarios/log/', postParams, auth)
      .then(data => {
        //console.log(data);
      })
      .catch(error => {
        console.log("error", error);
      });
    });
  }

  postColectivo(lat,lng,poste,recorrido,tiempo,distancia,version) {
    version = this.getVersion(version);
    let postParams =  {
         "sistema": 2,  // es el código de la app cuando llega
         "codigo_accion": "PIDEBONDI",
         "detalles": [
                    {"codigo": "latitud", "decimal_val": lat},
                    {"codigo": "longitud", "decimal_val": lng},
                    {"codigo": "poste_id", "int_val": poste},
                    {"codigo": "recorrido_id", "int_val": recorrido},
                    {"codigo": "error", "int_val": 0},
                    {"codigo": "detalle_error", "str_val": "ok, sin error"},
                    {"codigo": "demora_recibida_minutos", "decimal_val": tiempo},
                    {"codigo": "distancia_recibida_km", "decimal_val": distancia},
                    {"codigo": "Version", "str_val": version},
                  ]
        };
    /*const data = JSON.parse(localStorage.getItem("userData"));
    let headers = {"Authorization": "Token "+data.key,
                    "Content-Type": "application/json"};*/
    this.http.setDataSerializer("json");
    this.getAuthHeaders().then((auth) => {
      this.http.post('https://gobiernoabierto.cordoba.gob.ar/api/v2/log-usuarios/log/', postParams, auth)
        .then(data => {
          //console.log(data);

        })
        .catch(error => {
          console.log("error", error);
        });
    });
  }

  postColectivoError(lat,lng,poste,recorrido,error,detalle,version) {
    version = this.getVersion(version);
    let postParams =  {
         "sistema": 2,  // es el código de la app cuando llega
         "codigo_accion": "PIDEBONDI",
         "detalles": [
                    {"codigo": "latitud", "decimal_val": lat},
                    {"codigo": "longitud", "decimal_val": lng},
                    {"codigo": "poste_id", "int_val": poste},
                    {"codigo": "recorrido_id", "int_val": recorrido},
                    {"codigo": "error", "int_val": error},
                    {"codigo": "detalle_error", "str_val": detalle},
                    {"codigo": "demora_recibida_minutos", "int_val": 0},
                    {"codigo": "distancia_recibida_km", "int_val": 0},
                    {"codigo": "Version", "str_val": version},
                  ]
        }
    /*const data = JSON.parse(localStorage.getItem("userData"));
    let headers = {"Authorization": "Token "+data.key,
                    "Content-Type": "application/json"};*/
    this.http.setDataSerializer("json");
    this.getAuthHeaders().then((auth) => {
      this.http.post('https://gobiernoabierto.cordoba.gob.ar/api/v2/log-usuarios/log/', postParams, auth)
        .then(data => {
          //console.log(data);
        })
        .catch(error => {
          console.log("error", error);
        });
    });
  }

  postColectivoGrave(lat,lng,poste,recorrido,numError,error,version) {
    version = this.getVersion(version);
    let postParams =  {
         "sistema": 2,  // es el código de la app cuando llega
         "codigo_accion": "PIDEBONDI",
         "detalles": [
                    {"codigo": "latitud", "decimal_val": lat},
                    {"codigo": "longitud", "decimal_val": lng},
                    {"codigo": "poste_id", "int_val": poste},
                    {"codigo": "recorrido_id", "int_val": recorrido},
                    {"codigo": "error", "int_val": numError},
                    {"codigo": "detalle_error", "str_val": error},
                    {"codigo": "demora_recibida_minutos", "int_val": 0},
                    {"codigo": "distancia_recibida_km", "int_val": 0},
                    {"codigo": "Version", "str_val": version},
                  ]
        }
    /*const data = JSON.parse(localStorage.getItem("userData"));
    let headers = {"Authorization": "Token "+data.key,
                    "Content-Type": "application/json"}*/
    this.http.setDataSerializer("json");
    this.getAuthHeaders().then((auth) => {
      this.http.post('https://gobiernoabierto.cordoba.gob.ar/api/v2/log-usuarios/log/', postParams, auth)
          .then(data => {
            //console.log(data);

          })
          .catch(error => {
            console.log("error", error);
          });
    });
  }

  postValoracion(tipo,opinion,version) {
    version = this.getVersion(version);
    let postParams =  {
      "sistema": 2,  // es el código de la app cuando llega
      "codigo_accion": "OPINA",
      "detalles": [
                    {"codigo": "valoracion", "str_val": tipo},
                    {"codigo": "opinion", "str_val": opinion},
                    {"codigo": "Version", "str_val": version},
                  ]
        }
    /*const data = JSON.parse(localStorage.getItem("userData"));
    let headers = {"Authorization": "Token "+data.key,
                    "Content-Type": "application/json"};*/
          
    this.http.setDataSerializer("json");
    this.getAuthHeaders().then((auth) => {
      this.http.post('https://gobiernoabierto.cordoba.gob.ar/api/v2/log-usuarios/log/', postParams, auth)
        .then(data => {
          console.log(data);

        })
        .catch(error => {
          console.log(error);
        });
      });
  }

  /**
   * Envía la reacción de un usuario a un mensaje al API
   * @param codeID Código de reacción
   * @param msgData Objeto <UserMessage> con información del mensaje que se debe postear al API
   */
  postMessageReaction(codeID:number, msgData:UserMessage) {
    let postURL = "https://gobiernoabierto.cordoba.gob.ar/api/v2/mensajes/mensajes/reaccion_mensaje/";
    let postParams = {
        tipo: msgData.msgType,
        id: msgData.id,
        reaccion: codeID,
    }
    /*const userData = JSON.parse(localStorage.getItem("userData"));
    let headers = {"Authorization": "Token "+ userData.key, "Content-Type": "application/json"};*/

    this.http.setDataSerializer("json");
    
    this.getAuthHeaders().then((auth) => {
      this.http.post(postURL, postParams, auth).then(data => {
        console.log("[postMessageReaction Success] - ", JSON.stringify(data));
      }).catch(error => {
        console.error("[postMessageReaction Error] - ", JSON.stringify(error, Object.getOwnPropertyNames(error)));
      });
    });
  }

  /**
   * Obtiene una lista de mensajes enviados desde el API
   * Permite agregar parámetros extras (para evitar caches por ejemplo)
   */
  getMessagesList() : Promise<HTTPResponse> {
    return new Promise<HTTPResponse>((resolve) => {
      let postURL = "https://gobiernoabierto.cordoba.gob.ar/api/v2/mensajes/mensajes/";
    
      let postParams =  {
          "sistema": "2"    // Tiene que ser "string" para que funcione http.get()
      }
  
      /*if (avoid_cache) {
        let dt = String(new Date().getTime());
        postParams['extra'] = dt;
      }*/

      this.http.setDataSerializer("json");
      this.getAuthHeaders().then((auth) => {
        return resolve(this.http.get(postURL, postParams, auth));
      });
    });
  }

  postNovedadesParadas(novedades, paradaID, version){
    return new Promise<HTTPResponse>((resolve) => {
      version = this.getVersion(version);
      let postParams =  {
        "sistema": 2,  // es el código de la app cuando llega
        "codigo_accion": "NOTIFICACIONPARADA",
        "detalles": [
                      {"codigo": "parada_id", "str_val": paradaID},
                      {"codigo": "codigos_marcados", "str_val": novedades},
                      {"codigo": "Version", "str_val": version},
                    ]
          }
      
      this.http.setDataSerializer("json");
      this.getAuthHeaders().then((auth) => {
        return resolve(this.http.post('https://gobiernoabierto.cordoba.gob.ar/api/v2/log-usuarios/log/', postParams, auth));
      });
    });
  }
}
