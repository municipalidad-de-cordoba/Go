import { Injectable } from "@angular/core";
import { ToastController } from "ionic-angular";

// Cantidad de clicks necesarios para activar el modo desarrollador
const CLICKS_TO_ENABLE_DEV_MODE = 8;
const DEVMODE_STORAGE_KEY = "devMode";

@Injectable()
export class LoggerServiceProvider {
    /** @description Indica si el modo desarrollador está activo */
    private _isDevModeEnabled:boolean;

    /** @description Lista de logs a presentar en la app-info page */
    private _logList: Array<{text:string, icon:string}>;

    /** @description Cantidad de clicks realizados sobre el botón "Sobre esta app" */
    private _devButtonClickCount: number;

    /** @description Momento en el que se realizó el último click */
    private _lastTimeClicked: number;

    constructor(private toastCtrl: ToastController) {
        this._isDevModeEnabled = false;
        this._logList = [];
        this._devButtonClickCount = 0;
        this._lastTimeClicked = 0;

        this.init();
    }

    private init() {
        let devModeVal = localStorage.getItem(DEVMODE_STORAGE_KEY);

        this._isDevModeEnabled = devModeVal == null ? false : devModeVal.toLowerCase() === "true";
    }

    /**
     * @description Incrementa el contador de clicks y activa o desactiva el modo desarrollador.
     */
    public incrementDevButtonClickCount() {
        let currentTime = new Date().getTime();

        if (this._lastTimeClicked == 0) this._lastTimeClicked = currentTime;

        let timeDifference = currentTime - this._lastTimeClicked;

        // Pasó menos de 1 minuto desde la última vez que se clickeó
        if (timeDifference < 1000*60) {
            this._devButtonClickCount++;    // Se incrementa la cantidad de clicks
            
            // Se activa el modo desarrollador al llegar al número de clicks requeridos
            if (this._devButtonClickCount >= CLICKS_TO_ENABLE_DEV_MODE) 
            {
                if (!this.isDevModeEnabled())
                {
                    this.setDevMode(true);
                    this.dbLog("(!) El modo desarrollador ha sido activado.");
                    this.showAlertMessage("Modo desarrollador activado.");
                    
                    // Reiniciamos el contador de clicks.
                    this._devButtonClickCount = 0;

                    localStorage.setItem(DEVMODE_STORAGE_KEY, "true");
                }
                else
                {
                    this.setDevMode(false);
                    this.dbLog("El modo desarrollador ha sido desactivado.");
                    this.showAlertMessage("Modo desarrollador desactivado.");
                    
                    // Reiniciamos el contador de clicks.
                    this._devButtonClickCount = 0;

                    localStorage.setItem(DEVMODE_STORAGE_KEY, "false");
                }
            }
        }
        else this._devButtonClickCount = 0; // Se reinicia el contador 
        
        // Actualizamos el timestamp del último clickeo
        this._lastTimeClicked = currentTime;
    }

    /** @description Escribe un mensaje de información en el contenedor de logging.
     * @param msg Mensaje a escribir.
     */
    public dbLog(msg:any) {
        if (this.isDevModeEnabled()) {
            let dt = new Date().toLocaleTimeString();
            let logObj = {
                text: dt + ": " + msg,
                icon: "information-circle"
            };
    
            this._logList.push(logObj);
        }

        console.log(msg);
    }

    /** @description Escribe un mensaje de advertencia en el contenedor de logging.
     * @param msg Mensaje a escribir.
     */
    public dbWarn(msg:any) {
        if (this.isDevModeEnabled()) {
            let dt = new Date().toLocaleTimeString();
            let logObj = {
                text: dt + ": " + msg,
                icon: "warning"
            };

            this._logList.push(logObj);
        }

        console.warn(msg);
    }

    /** @description Escribe un mensaje de error en el contenedor de logging.
     * @param msg Mensaje a escribir.
     */
    public dbErr(msg:any) {
        if (this.isDevModeEnabled()) {
            let dt = new Date().toLocaleTimeString();
            let logObj = {
                text: dt + ": " + msg,
                icon: "alert"
            };

            this._logList.push(logObj);
        }

        console.error(msg);
    }

    /** @description Obtiene todos los logs registrados hasta el momento */
    public getLogsList() { return this._logList; }

    /**
     * @description Establece el modo desarrollador.
     * @param devStatus true = activar modo desarrollador, false = desactivar modo desarrollador.
     */
    public setDevMode(devStatus:boolean) { this._isDevModeEnabled = devStatus; }

    /**
     * @description Indica si el modo desarrollador está activo.
     */
    public isDevModeEnabled() { return (this._isDevModeEnabled) ? true : false; }

    /** @description Escribe un mensaje informativo en consola
     * @param msg Mensaje a escribir en la consola.
     */
    public cLog(msg: any, ...arParam: any[]) { console.log(msg, arParam); }

    /** @description Escribe un mensaje de advertencia en consola
     * @param msg Mensaje a escribir en la consola.
     */
    public cWarn(msg: any) { console.warn(msg); }

    /** @description Escribe un mensaje de error en consola
     * @param msg Mensaje a escribir en la consola.
     */
    public cErr(msg:any) { console.error(msg); }

    /** @description Muestra un mensaje en un tooltip en la parte inferior
     * @param msg Mensaje a escribir.
     */
    private showAlertMessage(msg:string) {
        let toast = this.toastCtrl.create({
            message: msg,
            duration: 3000,
            position: "bottom"
        });

        toast.onDidDismiss(() => { this.dbLog('Dismissed toast'); });
        toast.present();
    }
}
