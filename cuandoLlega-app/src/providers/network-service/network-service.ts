import { Injectable } from '@angular/core';
import { Network } from "@ionic-native/network/ngx";
import { Toast, ToastController } from "ionic-angular";

@Injectable()
export class NetworkServiceProvider {

  defaultToastOptions = {
    message: 'Compruebe su conexión a Internet...',
    position: 'top'
  };

  toast: Toast;

  constructor(private network: Network, private toastCtrl: ToastController) {
  }

  public toastNetworkDisconnected() {
    this.network.onchange().subscribe((event: Event) => {
      const wentOnline = event.type == 'online';
      const isToastPresented = !!this.toast;

      if (!(wentOnline || isToastPresented)) {
        this.createAndPresentToast();
        return;
      }

      if (isToastPresented) {
        this.toast.dismissAll();
      }

      this.toast = null;
    });
  }

  private createAndPresentToast() {
    this.toast = this.toastCtrl.create(this.defaultToastOptions);
    this.toast.present();
  }
}

