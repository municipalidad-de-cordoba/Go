/** Claves de almacenamiento para los distintos "tipos" de cache. */
export enum CacheType {
    DEVICE_ID = "DEVICE_ID",

    /** Corresponde al ID identificatorio del usuario. */
    USER_DATA = "userData",
    /** Corresponde al listado de fichas. */
    LISTA_FICHAS = "FichaFinal",
    /** Corresponde al listado de troncales. */
    TRONCALES = "cache_troncales",
    /** Corresponde al listado de recorridos de la linea seleccionada. */
    RECORRIDOS = "cache_recorridos",
    /** Corresponde al listado de postes del recorrido seleccionado. */
    POSTES_RECORRIDO = "cache_postes",

    /** Corresponde a la lista de configuraciones. */
    CONFIGURACIONES = "configuraciones",
    /** Corresponde a la Config. de Opciones de Paradas */
    CONFIG_NOTIF_NOV_PARADAS = "opciones_notificar_novedades_paradas",
    /** Corresponde a la Config. de Opciones de Colectivos */
    CONFIG_NOTIF_NOV_BONDI = "opciones_notificar_novedades_colectivos",
    /** Corresponde a la Config. de zoom minimo para mostrar paradas.*/
    CONFIG_MIN_ZOOM = "min_zoom_to_show_paradas",
    /** Corresponde a la Config. de Cant. Maxima de bondis en espera. */
    CONFIG_MAX_BONDI_ESPERA = "maximo_colectivos_en_espera",
    /** Corresponde a la Config. con el texto del header de la App. */
    CONFIG_TXT_APP_HEADER = "txt_app_header",
    
    /** Corresponde al codigo de seguimiento de una ficha. */
    CODIGO_INTERVAL = "tracking_interval_code",

    /** Corresponde a los colectivos esperados en la sección de tracking. */
    COLECTIVOS_ESPERADOS = "colectivos_esperados",

    /** Corresponde a la lista de favoritos. */
    LISTA_FAVORITOS = "listaFav",

    DEV_MODE = "DEV_MODE",

    /** Corresponde al tutorial. */
    TUTORIAL = "tutorial",
    
    /** Corresponde al sistema de mensajeria de usuarios. */
    USER_INBOX = "USER_INBOX",

    ZONAS_WIFI = "zonas_wifi",

    CLEAR_CACHE = "clearCache"
}

/** Claves relacionadas a la configuración de la App. */
export enum ConfigType {
    /** Corresponde a la info de configuración en si. */
    CONFIG_KEY = "configuraciones",
    /** Corresponde a la version minima necesaria. */
    CONFIG_VER_MIN = "version_minima",
    /** Corresponde a la version recomendada necesaria. */
    CONFIG_VER_REC = "version_recomendada",
    /** Texto a mostrar si no hay version minima. */
    CONFIG_TXT_VER_MIN = "txt_sino_hay_version_minima",
    /** Texto a mostrar si no hay version recomendada. */
    CONFIG_TXT_VER_REC = "txt_sino_hay_version_recomendada"
}