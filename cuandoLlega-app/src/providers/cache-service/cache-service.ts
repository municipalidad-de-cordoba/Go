import { Injectable } from "@angular/core";
import { CacheType } from "../cache-service/cache-enum";
import { Storage } from "@ionic/storage";

@Injectable()
export class CacheProvider {
    constructor(private storage:Storage) {}

    public setItemAsync(cache: CacheType, value:string): Promise<any> {
        let data =
        {
            ExpTime: this.setExpirationTime(cache),
            Value: value
        };

        return this.storage.set(cache, JSON.stringify(data));
    }

    /**
     * Obtiene información almacenada en memoria interna.
     * @param cache Clave de caché que se desea obtener.
     * @returns Devuelve una cadena con el contenido obtenido en memoria. Debe parsearse a objeto
     * desde el lugar donde es llamado.
     */
    public getItemAsync(cache: CacheType) : Promise<any> {
        return new Promise((resolve) => {
            this.storage.get(cache).then((tmpData) => {
                let parsed = JSON.parse(tmpData);
                return resolve(parsed !== null ? parsed["Value"] : null);
            });
        });
    }

    /**
     * Devuelve un array de objetos con las configuraciones principales de la App
     * almacenadas en memoria interna.
     */
    public getAppConfig(): Promise<string> {
        //let configData = JSON.parse(this.getItem(CacheType.CONFIGURACIONES));
        //return configData;
        return this.getItemAsync(CacheType.CONFIGURACIONES);
    }

    /**
     * Obtiene el valor de una configuración específica.
     * @param key Clave identificatoria de la configuración.
     */
    public getAppConfig_byKeyAsync(key: CacheType): Promise<any> {
        return new Promise((resolve) => {
            this.getAppConfig().then((tmpList) => {
                if (tmpList === null) return resolve(null);
                let curConfigValue: string;
                let configList = JSON.parse(tmpList);
    
                for (let i = 0; i < configList.length; i++) {
                    if (configList[i] == null) continue;
                    if (configList[i].Key == key) {
                        curConfigValue = configList[i].Value;
                        break;
                    }
                }
    
                return resolve(curConfigValue);
            });
        });
    }

    /**
     * Elimina información almacenada en memoria interna.
     * @param cache Clave de caché que se desea eliminar.
     */
    public removeItemAsync(cache: CacheType) {
        this.storage.remove(cache);
    }

    public removeAllAsync() {
        this.storage.clear();
    }

    /**
     * Verifica si el TTL de los datos almacenados para la clave especificada ha vencido.
     * @param cache Clave de caché a verificar
     */
    public isCacheOutdated(cache: CacheType) : Promise<boolean> {
        return new Promise((resolve) => {
            //let tmpData = JSON.parse(localStorage.getItem(cache));
            this.storage.get(cache).then((tmpData) => {
                // Si no hay info en cache, hay que obtenerla
                if (!tmpData) return resolve(true);
                let data = JSON.parse(tmpData);
                let cacheExpTime = parseInt(data["ExpTime"], 10);

                // Si el tiempo de expiración es "-1" quiere decir que el cache no tiene fecha
                // de expiración. Para eliminarse la información se debe llamar directamente a 
                // this.removeItem();
                if (cacheExpTime === -1) return resolve(false);

                let curTime = new Date().getTime();

                if (curTime >= cacheExpTime) return resolve(true);
                else return resolve(false);
            });
        });
    }

    private setItemTTL(cache: CacheType) {
        // Un valor de -1 representa un TTL infinito.
        let ttlTime:number = -1;
        let hour = 1000*60*60;
        let day = hour*24;

        switch (cache) {
            case CacheType.TUTORIAL:
            case CacheType.USER_DATA:
                ttlTime = -1;
                break;

            case CacheType.POSTES_RECORRIDO:
            case CacheType.RECORRIDOS:   // 1 día
                ttlTime = day*1;
                break;

            case CacheType.TRONCALES:   // 2 días
                ttlTime = day*2;
                break;

            case CacheType.ZONAS_WIFI:
                ttlTime = day*30;       // 1 mes
                break;

            default: // Por defecto: 2 horas.
                ttlTime = hour*2;
                break;
        }

        return ttlTime;
    }

    /**
     * Establece el tiempo de expiración de la información.
     * @param cache Clave de la información almacenada en memoria.
     */
    private setExpirationTime(cache: CacheType) {
        let curTTL = this.setItemTTL(cache);
        return curTTL !== -1 ? new Date().getTime() + curTTL : curTTL;
    }
}