import { Injectable } from "@angular/core";
import {
    GoogleMaps,
    GoogleMap,
    GoogleMapOptions
  } from '@ionic-native/google-maps';
import { LoggerServiceProvider } from "../logger-service/logger-service";

  @Injectable()
  export class MapProvider {
    /** Instancia de Google Map. */
    private map: GoogleMap = null;
    
    /** Latitud por defecto. */
    private curLat = -31.4201;
    /** Longitud por defecto. */
    private curLng = -64.1888;

    private mapLoaded: boolean = false;

    constructor(private logs: LoggerServiceProvider) { }

    public getInstance(elementID: string) {
        // Solo puede haber una sola instancia de google map.
        this.logs.dbLog("[!] Inicializando mapa...");

        /*if (!this.map)*/ return this.map = GoogleMaps.create(elementID, this.setGoogleMapOptions());
        /*else {
            return new Promise<GoogleMap>((resolve) => {
                console.warn("[Map] - Mapa ya fue creado. Instancia actual: ", this.map);
                resolve(this.map);
            });
        }*/

        /*return this.map.one(GoogleMapsEvent.MAP_READY).then(() => {
            this.map.off(GoogleMapsEvent.MAP_READY);
            return this.map;
        });*/
    }

    public isMapLoaded() { return this.mapLoaded; }

    public detachMap() {
        this.map.setDiv(null);
    }

    public attachMap(elementID: string) {
        this.map.setDiv(elementID);
    }

    /**
     * Configura y establece las opciones deseadas para el mapa de Google.
     * @returns Devuelve un objeto GoogleMapOptions con las configuraciones deseadas para el mapa de Google.
     */
    private setGoogleMapOptions() : GoogleMapOptions {
        console.log("setGoogleMapOptions()");
        let mapOptions: GoogleMapOptions = {
            gestures: {
                scroll: true,
                tilt: false,
                zoom: true,
                rotate: true,
            },
            preferences: {
                zoom: {
                minZoom: 11.5,
                maxZoom: 18
                }
            },

            styles: [
                {
                "featureType": "water",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#e9e9e9"
                    },
                    {
                        "lightness": 17
                    }
                ]
                },
                {
                "featureType": "landscape",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#f5f5f5"
                    },
                    {
                        "lightness": 20
                    }
                ]
                },
                {
                "featureType": "road.highway",
                "elementType": "geometry.fill",
                "stylers": [
                    {
                        "color": "#ffffff"
                    },
                    {
                        "lightness": 17
                    }
                ]
                },
                {
                "featureType": "road.highway",
                "elementType": "geometry.stroke",
                "stylers": [
                    {
                        "color": "#ffffff"
                    },
                    {
                        "lightness": 29
                    },
                    {
                        "weight": 0.2
                    }
                ]
                },
                {
                "featureType": "road.arterial",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#ffffff"
                    },
                    {
                        "lightness": 18
                    }
                ]
                },
                {
                "featureType": "road.local",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#ffffff"
                    },
                    {
                        "lightness": 16
                    }
                ]
                },
                {
                "featureType": "poi",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#f5f5f5"
                    },
                    {
                        "lightness": 21
                    }
                ]
                },
                {
                "featureType": "poi.park",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#c0ecae"
                    },
                    {
                        "lightness": 21
                    }
                ]
                },
                {
                "elementType": "labels.text.stroke",
                "stylers": [
                    {
                        "visibility": "on"
                    },
                    {
                        "color": "#ffffff"
                    },
                    {
                        "lightness": 16
                    }
                ]
                },
                {
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "saturation": 36
                    },
                    {
                        "color": "#333333"
                    },
                    {
                        "lightness": 40
                    }
                ]
                },
                {
                "elementType": "labels.icon",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
                },
                {
                "featureType": "transit",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#f2f2f2"
                    },
                    {
                        "lightness": 19
                    }
                ]
                },
                {
                "featureType": "administrative",
                "elementType": "geometry.fill",
                "stylers": [
                    {
                        "color": "#fefefe"
                    },
                    {
                        "lightness": 20
                    }
                ]
                },
                {
                "featureType": "administrative",
                "elementType": "geometry.stroke",
                "stylers": [
                    {
                        "color": "#fefefe"
                    },
                    {
                        "lightness": 17
                    },
                    {
                        "weight": 1.2
                    }
                ]
                }
            ],
            camera: {
                target: {
                lat: this.curLat,
                lng: this.curLng
                },
                zoom: 14
            },
        };
        return mapOptions;
    }
  }