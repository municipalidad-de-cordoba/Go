import { Injectable } from "@angular/core";
import { Events } from 'ionic-angular';
import { Storage } from "@ionic/storage";
import { UserMessage } from "../../app/globals";
import { AuthServiceProvider } from "../auth-service/auth-service";
import { LoggerServiceProvider } from "../logger-service/logger-service";

// Clave de memoria interna para obtener / almacenar los mensajes
const USER_INBOX_KEY = "USER_INBOX";

@Injectable()
export class UserInboxServiceProvider {
    /** @description Corresponde a la lista de mensajes */
    private msgList: Array<UserMessage> = [];

    constructor(private events:Events, private storage:Storage, private authService:AuthServiceProvider, private logService:LoggerServiceProvider) {
        this.msgList = [];
    }

    /**
     * Obtiene los mensajes desde el API y los carga a la colección de mensajes.
     */
    public getMessagesFromAPI() {
        this.logService.dbLog("Entrando a getMessagesFromAPI()");

        // Solicitamos al API la lista de mensajes
        this.authService.getMessagesList().then((response) => {
            this.logService.dbLog("[authService.getMessagesList()] - Solicitud de mensajes exitosa.");
            this.logService.dbLog("[authService.getMessagesList()] - Respuesta del api (response.data): " + response.data);

            // Obtenemos la respuesta del servidor y parseamos a objeto
            let data = JSON.parse(response.data);

            // Obtenemos la lista de mensajes
            let userMsgList = data["mensajes"];

            this.logService.dbLog("[authService.getMessagesList()] - Lista de mensajes: " + userMsgList);
            
            // Obtenemos los mensajes desde memoria interna
            this.storage.get(USER_INBOX_KEY).then((inboxData) => {
                this.logService.dbLog("[authService.getMessagesList()] - Obteniendo mensajes desde internal storage");

                if (inboxData != null) this.msgList = JSON.parse(inboxData);

                if (userMsgList != null) {
                    // Recorremos los mensajes recibidos
                    for (let i = 0; i < userMsgList.length; i++) {
                        let newMsg = userMsgList[i];

                        if (newMsg != null) {
                            // Añadimos un nuevo mensaje a la colección
                            this.addNewMessage(new UserMessage(newMsg.id, newMsg.titulo, newMsg.mensaje_html, newMsg.tipo, true));
                        }
                    }

                    // Al finalizar, almacenamos todos los mensajes en memoria interna
                    this.saveMessagesToStorage(this.msgList);
                }

                // Se publica el evento para que se actualicen la cantidad de mensajes sin leer en las demás vistas.
                this.events.publish("inbox:msgCount");

                this.logService.dbLog("[authService.getMessagesList()] - Mensajes obtenidos - Disparando evento 'inbox:msgCount'");
            });
        }).catch((error) => {
            // Es necesario llamar a getOwnPropertyNames para poder loguear correctamente el error recibido. De lo contrario
            // sólo se loguea "[Object] Object"
            let errorData = JSON.stringify(error, Object.getOwnPropertyNames(error));
            
            this.logService.dbErr("[authService.getMessagesList()] - Se produjo un error al procesar la solicitud: " + errorData);
        });
    }

    /**
     * Añade un nuevo mensaje a la colección de mensajes
     * @param message Mensaje a añadir
     */
    private addNewMessage(message:UserMessage) {
        let duplicated = false;

        // Nos aseguramos que por algún motivo el mensaje no se haya añadido con anterioridad.
        for (let i = 0; i < this.msgList.length; i++) {
            let curMsg = this.msgList[i];

            if (curMsg != null && curMsg.id == message.id) {
                duplicated = true;
                break;
            }
        }

        if (!duplicated) this.msgList.push(message);
    }

    /**
     * Almacena la lista de mensajes en memoria interna
     * @param messageList Lista de mensajes a almacenar
     */
    public saveMessagesToStorage(messageList:Array<UserMessage>) {
        this.storage.set(USER_INBOX_KEY, JSON.stringify(messageList));
    }

    /**
     * Envía una reacción al API
     * @param codeID Código de reacción.
     * @param msgID ID del mensaje en cuestión.
     */
    public sendMessageReaction(codeID:number, msgID:number) {
        let MsgDict = this.findMessage(msgID);

        if (MsgDict.CurrentMessage != null) this.authService.postMessageReaction(codeID, MsgDict.CurrentMessage);
        else {
            console.error("[sendMessageReaction error] - Por algún motivo el mensaje con ID: (" + msgID + ") es null.");
        }
    }

    /**
     * Obtiene un objeto que contiene un Objeto con el mensaje en cuestión y un índice que representa
     * la posición en la lista de mensajes en la cual el mismo fue encontrado.
     * @param msgID ID del mensaje a buscar
     */
    private findMessage(msgID:number) {
        let MessageDictionary = {
            CurrentMessage: null,
            index: -1
        }

        // Recorremos la lista de mensajes para encontrar el ID del mensaje a eliminar
        for (let i = 0; i < this.msgList.length; i++) {
            if (this.msgList[i] != null && this.msgList[i].id == msgID) {
                MessageDictionary.CurrentMessage = this.msgList[i];
                MessageDictionary.index = i;                
                break;
            }
        }

        return MessageDictionary;
    }

    /**
     * Elimina un mensaje de la lista de mensajes.
     * @param msgID ID del mensaje a eliminar
     */
    public deleteMessage(msgID:number) {
        let MsgDict = this.findMessage(msgID);

        if (MsgDict.CurrentMessage != null) {
            this.msgList.splice(MsgDict.index, 1);
            
            // Actualizamos la lista de mensajes
            this.saveMessagesToStorage(this.msgList);
        }

        // Se publica el evento para que se actualicen la cantidad de mensajes sin leer en las demás vistas.
        this.events.publish("inbox:msgCount");
    }

    /**
     * @description Obtiene la cantidad de mensajes sin leer.
     */
    public getUnreadMessagesCount() {
        let unreadCount = 0;

        // Contamos la cantidad de elementos que no han sido leidos aún.
        this.msgList.forEach(element => { if (element.unread) unreadCount++; });
    
        return unreadCount;
    }

    /**
     * @description Obtiene la lista de mensajes del usuario
     */
    public getUserMessages() { return this.msgList; }
}