import { Recorrido, Coordinates } from './../../app/globals';
import { ApiServiceProvider } from './../api-service/api-service';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { of } from 'rxjs/observable/of';
import { CacheProvider } from '../cache-service/cache-service';
import { CacheType } from '../cache-service/cache-enum';

@Injectable()
export class RecorridosServiceProvider {

  recorridos:Recorrido[] = [];
  responseData : any;
  auxCoords : Coordinates[] = [];

  constructor(public http: HttpClient, private apiService:ApiServiceProvider, private cache: CacheProvider) {
    this.cache.getItemAsync(CacheType.RECORRIDOS).then((tmpData) => {
      if (tmpData) this.recorridos = JSON.parse(tmpData);
    });
  }

  getAllRecorridos() {
    return this.recorridos;
  }

  getRecorrido(id:string) {
    for (let i = 0; i < this.recorridos.length; i++) {
      if (this.recorridos[i].id == id) {
        return this.recorridos[i];
      }
    }
    return null;
  }

  getRecorridos(linea:string): Observable<Recorrido[]> {
    //if (this.recorridos == null) return this.apiService.getRecorridos(linea);

    let listRecorridos:Recorrido[] = [];
    for (let i = 0; i < this.recorridos.length; i++) {
      // Nos aseguramos de filtrar los duplicados antes de mostrar el listado de 
      // recorridos al usuario.
      if (this.recorridos[i].linea.nombre == linea && !this.exists(this.recorridos[i])) listRecorridos.push(this.recorridos[i]);
    }
    if (listRecorridos.length > 0) return of(listRecorridos);
    else return this.apiService.getRecorridos(linea);
  }

  exists(r:Recorrido) {
    if (this.recorridos == null) return false;

    for (let i = 0; i < this.recorridos.length; i++) {
      if (this.recorridos[i].id == r.id) return true;
    }
    return false;
  }

  setRecorrido(r:Recorrido) {
    if (this.exists(r)) {
      this.recorridos = this.recorridos.filter(item => item !== r);
    }
    this.recorridos.push(r);

    this.cache.setItemAsync(CacheType.RECORRIDOS, JSON.stringify(this.recorridos));
  }

}

