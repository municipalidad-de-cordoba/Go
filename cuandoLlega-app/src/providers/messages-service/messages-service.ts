import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ToastController } from 'ionic-angular';

/*
  Generated class for the MessagesServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class MessagesServiceProvider {
  private toast : any;
  constructor(public http: HttpClient, public toastCtrl: ToastController) {
    // console.log('Hello MessagesServiceProvider Provider');
  }

  presentToast(message:string, position:string, closeButton?:boolean, duration?:number){
    
    this.toast  = this.toastCtrl.create({message: message, position:position});
    if(closeButton!=null){
      this.toast = this.toastCtrl.create({message: message, position:position, showCloseButton:true, closeButtonText: 'Cerrar'});
    }
    if(duration!=null){
      this.toast = this.toastCtrl.create({message: message, position:position, duration: duration});
    }

    this.toast.present();
  }
}
