import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Seguimiento, Favorito } from './../../app/globals'; 
/*
  Generated class for the TrackingServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class TrackingServiceProvider {

  toTrack : Seguimiento [] = [];
  favoritos : Favorito[] =[];

  constructor(public http: HttpClient) {
    console.log('Hello TrackingServiceProvider Provider');
  }
  /** Obtiene los datos de todos los combos (recorrido + poste) a seguir. */
  getAllTracks(){
    return this.toTrack;
  }
  /** Obtiene todos los datos de un combo (recorrido + poste) a seguir. */
  getTrack(track:Seguimiento){
    let auxTrack = null;
    this.toTrack.forEach(function (tr) {
      if(tr.parada == track.parada){
        // tr.tiempo = track.tiempo;
        // tr.tRestante = track.tRestante;
        auxTrack = tr;
      }
    }); 
    return auxTrack;
  }
  updateTrack(track:Seguimiento){
    
    this.toTrack.forEach(function (tr) {
      if(tr.parada == track.parada){
        tr.tRestanteSecs = track.tiempo;
      }
    }); 
  }
  

  /** Agrega un combo (recorrido + poste) a seguir. Si ya existe, lo actualiza */
  addTrack(track:Seguimiento){
    console.log("AGREGADO");
    let exists = false;
    this.toTrack.forEach(function (tr) {
      if(tr.parada == track.parada){
        tr.tiempo = track.tiempo;
        // tr.tRestante = track.tRestante;
        exists = true;
      }
    }); 

    if(!exists){
      this.toTrack.push(track);
    }
  }
  /** Elimina un combo (recorrido + poste).*/
  removeTrack(track:Seguimiento){
    this.toTrack = this.toTrack.filter(t => t.parada !== track.parada );
  }


  getAllFavorites(){
    return this.favoritos;
  }
  /** Obtiene todos los datos de un combo (recorrido + poste) a seguir. */
  getFavorito(favorito:Favorito):Favorito{
    let auxFav = null;
    this.favoritos.forEach(function (fav) {
      if(fav.parada.id == favorito.parada.id){
        auxFav = fav;
      }
    }); 
    return auxFav;
  }

  /** Agrega un combo (recorrido + poste) a seguir. Si ya existe, lo actualiza */
  addFavorito(favorito:Favorito){
    let exists = false;
    this.favoritos.forEach(function (fav) {
      if(fav.parada == favorito.parada){
        exists = true;
      }
    }); 
    //Agrego solo si ya no existe
    if(!exists){
      this.favoritos.push(favorito);
    }
  }

  /** Toggle de favorito */
  toggleFavorito(favorito:Favorito){
    let exists = false;
    this.favoritos.forEach(function (fav) {
      if(fav.parada == favorito.parada){
        this.favoritos = this.favoritos.filter(f => f.parada !== favorito.parada );
        exists = true;
        return this.favoritos;
      }
    }); 
    //Agrego solo si ya no existe
    if(!exists){
      this.favoritos.push(favorito);
      return this.favoritos;
    }
  }

  /** Elimina un combo (recorrido + poste).*/
  removeFavorito(favorito:Favorito){
    this.favoritos = this.favoritos.filter(f => f.parada !== favorito.parada );
  }


}
