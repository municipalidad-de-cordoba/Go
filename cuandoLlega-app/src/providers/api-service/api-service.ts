import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { DomSanitizer } from '@angular/platform-browser';
import { Globals } from '../../app/globals'
import { ITrackingResponseData } from '../../shared/model/traking-response-data.model';
import { Observable } from 'rxjs';
import { IDatosInternosResponseData } from "../../shared/model/datos-internos-response-data.model";

@Injectable()
export class ApiServiceProvider {

  protected trackingBaseUrl : string = 'https://cordobus.apps.cordoba.gob.ar/data/distancia_y_tiempo/';
  protected infoUrl : string = 'https://gobiernoabierto.cordoba.gob.ar/api/v2/software-municipal/apps-moviles/2/';
  protected redBus: string ='https://www.red-bus.com.ar/genxml.php?barrio=';
  protected zonaswifi : string ='https://gobiernoabierto.cordoba.gob.ar/api/v2/zonas/zonas/?categorias=1';
  protected datosInternosUrl : string ='https://cordobus.apps.cordoba.gob.ar/data/datos-de-internos/';
  protected lineasAPIUrl : string ='https://cordobus.apps.cordoba.gob.ar/lineas/api/';
  protected paradasAPIUrl : string ='https://cordobus.apps.cordoba.gob.ar/paradas-colectivos/api/';
  
  constructor(public http: HttpClient , public sanitizer: DomSanitizer, private globals: Globals) {
    // console.log('Hello ApiServiceProvider Provider');
  }

  getTroncales() {
    return this.http
      .get<any[]>(this.lineasAPIUrl + "lineas-troncales/?page_size="+this.globals.page_size+"")
      .pipe(map(data => data));
  }

  getLineas(troncal?:string) {
    return this.http
      .get<any[]>(this.lineasAPIUrl + "linea/?page_size="+this.globals.page_size+(troncal!=null?("&id_trocal="+troncal) :""))
      .pipe(map(data => data));
  }

  getRecorridos(linea:string) {
    return this.http
      .get<any[]>(this.lineasAPIUrl + "recorrido/?page_size="+this.globals.page_size+"&linea="+linea)
      .pipe(map(data => data));
  }

  /**
   * Obtiene la cantidad de unidades activas disponibles para 
   * @param idTroncal ID del troncal
   */
  getUnidadesActivas(idTroncal:string) {
    return this.http
    .get<any[]>(this.lineasAPIUrl + "activos-por-linea/?id_troncal="+idTroncal)
    .pipe(map(data => data));
  }

  getParadas(poste_id:string, linea:string) {
    return this.http
      .get<any[]>(this.paradasAPIUrl + "paradas-colectivos/?page_size="+this.globals.page_size+"&poste_id="+poste_id+(linea!=null?("&linea="+linea) :""))
      .pipe(map(data => data));
  }

   getPostes(bounds:string, linea?:string){
  //  getPostes(bounds:string, recorrido?:string){
    return this.http
      .get<any[]>(this.paradasAPIUrl + "postes-paradas-colectivos/?page_size="+this.globals.page_size+"&in_bbox="+bounds+(linea!=null?("&linea="+linea) :""))
      .pipe(map(data => data));
  }
  getPostesRecorrido(recorrido:string, bounds:string){
    return this.http
      .get<any[]>(this.paradasAPIUrl + "postes-paradas-colectivos/?page_size="+this.globals.page_size+"&recorrido_id="+recorrido+"&in_bbox="+bounds)
      .pipe(map(data => data));
  }

  getPosteSelect(poste:string){
    return this.http
      .get<any[]>(this.paradasAPIUrl + "postes-paradas-colectivos/?poste_id="+poste)
      .pipe(map(data => data));
  }

  getTracking(recorrido: string, poste: string): Observable<ITrackingResponseData[]> {
    // console.log("recorrido: "+recorrido);
    // console.log("poste: "+poste);
    // console.log("url: "+this.trackingBaseUrl + recorrido+"/"+poste);
    return this.http
      .get<ITrackingResponseData[]>(this.trackingBaseUrl + recorrido + "/" + poste)
      .pipe(map(data => data));
  }

  getTrackingTest(){
    return this.http
    .get<any[]>(this.trackingBaseUrl + "809/6109")
    .pipe(map(data => data));
  }

  getInfoApp() {
    return this.http.get<any[]>(this.infoUrl).pipe(map(data => data));
  }

  getRedBus(){
    console.log("entro a apiservice REDBUS")
    return this.http
    .get<any[]>(this.redBus)
    .pipe(map(data => data));
  }


  //zonas wifi
  getZonasWifi()
  {
    return this.http
    .get<any[]>(this.zonaswifi)
    .pipe(map(data => data));
  }

  getDatosInternos() {
    /* EJEMPLO
    [
      {
          "registrados": 593,
          "no_registrados": 29,
          "en_punta_de_lineas": 271,
          "adaptados_activos": 196,
          "adaptados_en_servicio": 259
      }
  ]
    */
    return this.http
      .get<IDatosInternosResponseData[]>(this.datosInternosUrl)
      .pipe(map(data => data));
  }
}
