import { Injectable } from "@angular/core";
import { TextToSpeech } from "@ionic-native/text-to-speech/ngx";
import { MobileAccessibility } from "@ionic-native/mobile-accessibility/ngx";

@Injectable()
export class TTSProvider {
    constructor(private tts: TextToSpeech, private accessibility: MobileAccessibility){}

    public speak(msg:string): Promise<any> {
        // Sólo para probar sin necesidad de tener ScreenReader activo en el dispositivo.
        /*return new Promise<any>((resolve) => {
            this.tts.speak("");

            resolve(this.tts.speak({
            text: msg,
            locale: "es-AR"
            }).catch((error) => console.error(error)));
        });*/

        return new Promise<any>((resolve) => {
            this.accessibility.isScreenReaderRunning().then((isReaderRunning) => {
                if (isReaderRunning == true) {
                    // Silenciamos TTS pasando una cadena vacía como parámetro
                    this.tts.speak("");

                    resolve(this.tts.speak({
                        text: msg, 
                        locale: "es-AR"
                    }).catch((error) => console.error(error)));
                }
                else resolve();
            })
        });
    }

    public stop(): Promise<any> {
        return new Promise<any>(() => {
            this.tts.speak("");
        });
    }
}