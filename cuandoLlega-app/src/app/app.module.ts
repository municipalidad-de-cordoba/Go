import { FavoritosPage } from './../pages/favoritos/favoritos';
import { ValoracionPage } from './../pages/valoracion/valoracion';
import { AppInfoPage } from './../pages/app-info/app-info';
import { TrackingPage } from './../pages/tracking/tracking';
import { LoginPageModule } from './../pages/login/login.module';
import { ComponentsModule } from './../components/components.module';

import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { CuandoLlegaApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { WelcomePage } from '../pages/welcome/welcome';
import { RedbusPage } from '../pages/redbus/redbus';
import { UserInboxPage } from '../pages/user-inbox/user-inbox';
import { UserInboxMsgPage } from '../pages/user-inbox/user-inbox-msg';

import { StatusBar } from '@ionic-native/status-bar/ngx';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { HttpClientModule } from '@angular/common/http';

//Providers
import { ApiServiceProvider } from '../providers/api-service/api-service';
import { AuthServiceProvider } from '../providers/auth-service/auth-service';
import { MessagesServiceProvider } from '../providers/messages-service/messages-service';
import { NetworkServiceProvider } from "../providers/network-service/network-service";
import { LoggerServiceProvider } from "../providers/logger-service/logger-service";

import { Globals } from './globals';

//Para guardar los datos
import { IonicStorageModule } from '@ionic/storage';
import { Network } from '@ionic-native/network/ngx';
import { RegisterPageModule } from '../pages/register/register.module';
import { ForgotPasswordPageModule } from '../pages/forgot-password/forgot-password.module';
import { TwitterConnect } from '@ionic-native/twitter-connect/ngx';
import { GoogleMaps } from '@ionic-native/google-maps/ngx';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { TapticEngine } from '@ionic-native/taptic-engine/ngx';

import { HttpBackend, HttpXhrBackend } from '@angular/common/http';
import { NativeHttpModule, NativeHttpBackend, NativeHttpFallback } from 'ionic-native-http-connection-backend';
import { Platform } from 'ionic-angular';
import { RecorridosServiceProvider } from '../providers/recorridos-service/recorridos-service';
import { TrackingServiceProvider } from '../providers/tracking-service/tracking-service';
import { EstadoServicioServiceProvider } from "../providers/estado-servicio-service/estado-servicio-service";
import { AppVersion } from '@ionic-native/app-version/ngx';
import { GooglePlus } from '@ionic-native/google-plus/ngx';
import { UserInboxServiceProvider } from '../providers/user-inbox-service/user-inbox-service';
import { CacheProvider } from '../providers/cache-service/cache-service';
import { MapProvider } from '../providers/map-service/map-service';
import { RoundProgressModule } from 'angular-svg-round-progressbar';
import { TextToSpeech } from '@ionic-native/text-to-speech/ngx';
import { MobileAccessibility } from "@ionic-native/mobile-accessibility/ngx";
import { TTSProvider } from '../providers/tts-service/tts-provider';

@NgModule({
  declarations: [
    CuandoLlegaApp,
    HomePage,
    WelcomePage,
    AppInfoPage,
    TrackingPage,
    FavoritosPage,
    ValoracionPage,
    RedbusPage,
    UserInboxPage,
    UserInboxMsgPage
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    IonicModule.forRoot(CuandoLlegaApp),
    IonicStorageModule.forRoot(),
    RegisterPageModule,
    LoginPageModule,
    ForgotPasswordPageModule,
    ComponentsModule,
    NativeHttpModule,
    RoundProgressModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    CuandoLlegaApp,
    HomePage,
    WelcomePage,
    AppInfoPage,
    TrackingPage,
    FavoritosPage,
    ValoracionPage,
    RedbusPage,
    UserInboxPage,
    UserInboxMsgPage
  ],
  providers: [
    Globals,
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    {provide: HttpBackend, useClass: NativeHttpFallback, deps: [Platform, NativeHttpBackend, HttpXhrBackend]},
    AuthServiceProvider,
    ApiServiceProvider,
    MessagesServiceProvider,
    NetworkServiceProvider,
    TwitterConnect,
    GoogleMaps,
    Network,
    Geolocation,
    TapticEngine,
    RecorridosServiceProvider,
    TrackingServiceProvider,
    EstadoServicioServiceProvider,
    AppVersion,
    GooglePlus,
    LoggerServiceProvider,
    UserInboxServiceProvider,
    CacheProvider,
    MapProvider,
    TextToSpeech,
    MobileAccessibility,
    TTSProvider
  ]
})
export class AppModule {}
