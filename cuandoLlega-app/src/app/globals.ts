import { Injectable } from "@angular/core";
import {  Marker } from '@ionic-native/google-maps';
import { Storage } from '@ionic/storage';

@Injectable()
export class Globals {

  page_size : number= 300;
  count_log : number = 0;

}


export class Poste {
  id: string;
  id_externo : string;
  descripcion: string;
  sentido: string;
  calle_principal: string;
  calle_secundaria: string;
  marker: Marker;

  /** Indica si el poste en cuestión ya ha sido notificado al servidor mediante el código "PIDE BONDI". */
  statCodeSent: boolean;

  constructor(id: string, id_externo: string, descripcion: string,  sentido: string, calle_principal: string, calle_secundaria: string, marker: Marker) {
    this.id = id;
    this.id_externo = id_externo;
    this.descripcion = descripcion;
    this.sentido = sentido;
    this.calle_principal = calle_principal;
    this.calle_secundaria = calle_secundaria;
    this.marker = marker;
    this.statCodeSent = false;
  }
}

export class Bus {
  id: string;
  marker: Marker;

  constructor(id: string, marker: Marker) {
    this.id = id;
    this.marker = marker;
  }
}

export class Troncal{
  id:string;
  nombre:string;
  lineas: Linea[];
  overflow:boolean;
  constructor(id:string, nombre:string, lineas:Linea[], overflow?:boolean){
    this.id = id;
    this.nombre = nombre;
    this.lineas = lineas;
    if(overflow){
      this.overflow = overflow;
    }else{
      this.overflow = false;
    }
  }
}

export class Linea{
  id: string;
  id_externo : string;
  troncal: string;
  nombre: string;
  empresa: string;
  id_empresa: string;
  unidadesActivas: number;

  constructor(id: string, id_externo: string, troncal:string, nombre: string,  empresa: string, id_empresa: string) {
    this.id = id;
    this.id_externo = id_externo;
    this.troncal = troncal;
    this.nombre = nombre;
    this.empresa = empresa;
    this.id_empresa = id_empresa;
    this.unidadesActivas = -1;
  }
}

export class Recorrido {
  id: string;
  linea : Linea;
  id_externo: string;
  descripcion_corta: string;
  descripcion_larga: string;
  geometry: RecGeometry;

  constructor(id: string, id_externo: string, linea: Linea,  descripcion_corta: string, descripcion_larga: string, geometry: RecGeometry) {
    this.id = id;
    this.id_externo = id_externo;
    this.linea = linea;
    this.descripcion_corta = descripcion_corta;
    this.descripcion_larga = descripcion_larga;
    this.geometry = geometry;
  }
}
export class Coordinates{
  lat:number;
  lng: number;
  constructor(lat:number, lng:number){
    this.lat = lat;
    this.lng = lng;
  }
}

export class RecGeometry{
  type: string;
  coordinates:Coordinates[];
  constructor(type:string, coordinates:Coordinates[]){
    this.type = type;
    this.coordinates = coordinates;
  }
}

export class Favorito{
  // recorrido:Recorrido;
  // poste:Poste;
  // constructor(recorrido:Recorrido, poste:Poste){
  //   this.recorrido = recorrido;
  //   this.poste = poste;
  // }

  parada:Parada;
  constructor(parada:Parada){
    this.parada = parada;
  }
}

export class Parada{
  linea:Linea;
  poste:Poste;
  id:string;
  constructor(id:string, linea:Linea, poste:Poste){
    this.id = id;
    this.linea = linea;
    this.poste = poste;
  }
}

//PASAR TODO LO DE ABAJO A UN COMPONENTE
export class Seguimiento{
  parada:number;
  distancia:number;
  // // recorrido:Recorrido;
  tiempo:number;
  // tRestante:number;
  tRestanteSecs:number;
  // // poste:Poste;
  // initClock:boolean;
  // tickPosition:number;

  timer:any;
  // taptic:TapticEngine;
  // tick:boolean; //Para animacion de los minutos

  constructor(parada:number, tiempo:number, distancia:number){
    this.parada = parada;
    this.tiempo = tiempo;
    this.distancia = distancia;
    this.tRestanteSecs = this.tiempo*60;
  }
  //   this.tRestante = this.tiempo;
  //   this.tRestanteSecs = this.tRestante * 60;
  //   this.initClock = false;
  //   this.taptic = taptic;
  //   this.tick = false;

  //   let t = this.tRestante - 1;
  //   if(t<19){
  //     this.tickPosition = t+11;
  //   }else if (t>=19 && t<50){
  //     this.tickPosition = t-19;
  //   }else if(t>=50 && t<59){
  //     this.tickPosition = t-49;
  //   }
  // }
  // startTimer(){
  //   setTimeout (() => {
  //     this.initClock=true; //Esperando que se cargue el reloj
  //   }, 2000);
  //   let tAnt = this.tRestanteSecs;
  //   this.timer = Observable.interval(1000).subscribe(x => {
  //     if(this.initClock){
  //       if(this.tRestanteSecs>0){
  //         console.log(this.tRestanteSecs);
  //         // if(this.tRestanteSecs==5){
  //         //   this.tick = true;
  //         //   console.log("TICK ON");
  //         //   setTimeout (() => {
  //         //     this.tick = false;
  //         //     console.log("TICK OFF");
  //         //   }, 500);
  //         // }
  //         if(this.tRestanteSecs==1){
  //           setTimeout (() => {
  //             this.tRestanteSecs = 0;
  //             this.tRestante = 0;
  //             this.taptic.notification({type: 'success'});

  //           }, 1000);
  //         }else{
  //           if(tAnt - this.tRestanteSecs == 60){
  //             this.tRestante -= 1;
  //             tAnt = this.tRestanteSecs;
  //           }
  //           this.tRestanteSecs = this.tRestanteSecs - 1;
  //         }

  //       }else{
  //         this.timer.unsubscribe();
  //       }
  //     }
  //   });
  // }

  // isFavorite(){

  // }
}

export class SeguimientoDistanciaTiempo{
  tiempo: number;
  distancia: number;
  descripcion: string;
  colectivo: any;
  id_recorrido: string;
  id_parada: string;
  guardar : any;
  ficha :any;

  guardarStorage : Storage;
  constructor(tiempo:number, distancia:number, descripcion:string, colectivo: any, id_recorrido: string, id_parada: string, ficha: string){
    this.tiempo = tiempo;
    this.distancia = distancia;
    this.descripcion = descripcion;
    this.colectivo = colectivo;
    this.id_recorrido = id_recorrido;
    this.id_parada = id_parada;
    this.ficha = ficha;
  }

  /*guardarSeguimiento(){
    this.guardarStorage.set('tiempo', "20");

    this.guardarStorage.get('tiempo').then((val) => {
      console.log('Your TIEMPO is', val);
    });
  }*/
  delete(){
    //delete this.p;
    // delete this.tiempo;
    // delete this.distancia;
    // delete this.descripcion;
    // delete this.colectivo;
    // delete this.id_recorrido;
    // delete this.id_parada;
    // delete this.ficha;
    console.log("delete ENTROOO ")
    }


}

/** 
 * @description Representa a un mensaje / notificación recibida desde el API
 */
export class UserMessage {
  /** @description ID interno del mensaje.  */
  id: number;
  /** @description Titulo del mensaje. */
  title: string;
  /** @description Texto del mensaje. */
  msgTxt: string;
  /** @description Tipo del mensaje (Grupal / Directo) */
  msgType: string;
  /** @description Indica si el mensaje ha sido leido. */
  unread: boolean;

  constructor(id:number, title:string, msgTxt:string, msgType:string, unread:boolean) {
    this.id = id;
    this.title = title;
    this.msgTxt = msgTxt;
    this.msgType = msgType;
    this.unread = unread;
  }
}