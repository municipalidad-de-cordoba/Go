import { FavoritosPage } from './../pages/favoritos/favoritos';
import { ValoracionPage } from './../pages/valoracion/valoracion';
import { TerminosYCondicionesPage } from './../pages/terminos-y-condiciones/terminos-y-condiciones';
import { WelcomePage } from './../pages/welcome/welcome';
import { TrackingPage } from '../pages/tracking/tracking';
import { AppInfoPage } from '../pages/app-info/app-info';
import { HomePage } from '../pages/home/home';
import { RedbusPage } from '../pages/redbus/redbus';
import { UserInboxPage } from '../pages/user-inbox/user-inbox';

import { Component, ViewChild } from '@angular/core';
import { Alert, AlertController, Nav, Platform, Events } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';

import { Network } from '@ionic-native/network/ngx';

import { ApiServiceProvider } from './../providers/api-service/api-service';
import { MessagesServiceProvider } from './../providers/messages-service/messages-service';
import { NetworkServiceProvider } from "../providers/network-service/network-service";
import { UserInboxServiceProvider } from "../providers/user-inbox-service/user-inbox-service";
 
import { HTTP } from '@ionic-native/http/ngx';

import { timer  } from 'rxjs/observable/timer';
import { AppVersion } from '@ionic-native/app-version/ngx';

import { CacheType, ConfigType } from "../providers/cache-service/cache-enum";
import { CacheProvider } from '../providers/cache-service/cache-service';
import { Subscription } from 'rxjs';

@Component({
  templateUrl: 'app.html'
})
export class CuandoLlegaApp {
  @ViewChild(Nav) nav: Nav;

  title = 'app';
  restItems: any;
  connected:boolean = true;

  /** Listado de páginas ubicadas en la parte superior del side menu principal. */
  pages: Array<{title: string, component: any}>;
  /** Listado de páginas ubicadas en la parte inferior del side menu principal. */
  pagesBottom: Array<{title: string, component: any}>;

  //public userDetails:any = {'first_name':"", 'las_name':""};
  rootPage:any;

  showSplash = true;

  /** Cantidad de mensajes que el usuario tiene sin leer. */
  _unreadMsgCount:number = 0;

  private _splashTimer: Subscription;

  constructor(public platform: Platform, private http: HTTP, public statusBar: StatusBar, public splashScreen: SplashScreen,
    private apiService: ApiServiceProvider, private network: Network,
    private messagesService: MessagesServiceProvider, private appVersion: AppVersion, public alertCtrl: AlertController, 
    private networkService: NetworkServiceProvider, private inboxService: UserInboxServiceProvider, private events: Events,
    private cache: CacheProvider) {

    // Comenzamos con la inicialización de la App.
    this.initializeApp();
 
    this.pages = [
      { title: 'Mensajes', component:UserInboxPage },
      { title: 'Paradas en espera', component:TrackingPage},
      { title: 'Favoritos', component:FavoritosPage },
      { title: 'Valoraciones', component:ValoracionPage },
      { title: 'Zona de carga de RedBus', component:RedbusPage},
      { title: 'Tutorial', component:HomePage}
    ];
    this.pagesBottom = [
      { title: 'Sobre esta App', component:AppInfoPage },
      { title: 'Cerrar Sesión', component: null}
    ];

    this.cache.getItemAsync(CacheType.DEVICE_ID).then((valID) => {
      if (valID !== null) {
        let unid = this.uid6d() + '-' + this.uid6d() + '-' + this.uid6d() + '-' + this.uid6d();
        this.cache.setItemAsync(CacheType.DEVICE_ID, JSON.stringify(unid));
      }
    });
  }

  /**
   * Realiza las operaciones necesarias para el funcionamiento de la App.
   */
  initializeApp() {
    // Esperamos que la plataforma esté lista.
    this.platform.ready().then(() => {
      this.setInitialRootPage();
      this.getBaseItems();

      this.statusBar.backgroundColorByHexString( '#f5f5f5');
      this.statusBar.overlaysWebView(true);

      this.http.get('https://gobiernoabierto.cordoba.gob.ar/api/', {}, {})
      .then(data => {
        console.log('cordova-plugin-advanced-http is installed properly');
      })
      .catch(error => {
        console.log(error.status);
        console.log(error.error); // error message as string
        console.log(error.headers);
      });
      this.networkService.toastNetworkDisconnected();
      
      // Debido al life-cycle de 'app.component.ts' es necesario escuchar por eventos para poder actualizar el contador
      // correctamente en el menú.
      this.events.subscribe("inbox:msgCount", () => {
        console.log("component.ts - inbox:msgCount");
        // Obtenemos la cantidad de mensajes sin leer del usuario.
        this._unreadMsgCount = this.inboxService.getUnreadMessagesCount();
      });
 
      this.splashScreen.hide(); 
      this._splashTimer = timer(1000).subscribe(() => { 
        this.showSplash = false;
        this._splashTimer.unsubscribe();
        this._splashTimer = undefined;
      });
    });
  }

  /**
   * Establece la página inicial basada en la información de usuario obtenida.
   */
  setInitialRootPage() {
    // Para la versión final
    // this.storage.get('userData').then((appData) => {
    //   if(appData==null){
    //     this.rootPage = WelcomePage;
    //   }else{ 
    //     this.rootPage = HomePage;
    //   }
    // });

    if (!this.clearUserCache()) {
      this.cache.getItemAsync(CacheType.USER_DATA).then((userData) => {
        if (userData != null && JSON.parse(userData).key) this.rootPage = HomePage;
        else {
          this.cache.removeItemAsync(CacheType.USER_DATA);
          this.cache.removeItemAsync(CacheType.LISTA_FICHAS);
          this.rootPage = WelcomePage;
        }
      });
    }
    else {
      this.rootPage = WelcomePage;
    }
  }

  /**
   * Abre / pushea una nueva vista en el stack.
   * @param page Página que se desea abrir
   */
  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    if (page.component) {
      if (page.component == HomePage) {
        let tutorial = 1;
        this.cache.setItemAsync(CacheType.TUTORIAL, JSON.stringify(tutorial));
        //this.nav.setRoot(page.component);
        this.events.publish("tutorialEvent", tutorial);
      }
      if (this.nav.getActive().name != page.component.name) {
        if (page.component == TerminosYCondicionesPage || TrackingPage) {
          this.nav.setRoot(page.component,{}, {animate: true, direction: 'back'});
        }
        else { 
          this.nav.setRoot(page.component);
        }
      }
    }
    else {
      this.userLogout();
    }
  }

  /**
   * Realiza operaciones de limpieza cuando el usuario cierra sesión.
   */
  private userLogout() {
    this.cache.removeItemAsync(CacheType.USER_DATA);
    this.cache.removeItemAsync(CacheType.CODIGO_INTERVAL);
    this.cache.removeItemAsync(CacheType.LISTA_FICHAS);
    this.cache.removeItemAsync(CacheType.COLECTIVOS_ESPERADOS);
    this.cache.removeItemAsync(CacheType.DEVICE_ID);
    this.cache.removeItemAsync(CacheType.TUTORIAL);

    this.nav.setRoot(WelcomePage);
  }

  private clearUserCache() {
    let shouldClean = localStorage.getItem(CacheType.CLEAR_CACHE);
    
    // Si el valor del cache es 1, limpiaremos todo el cache.
    if (shouldClean == null || shouldClean == "0") {
      console.log("Limpiando todo el cache...");

      this.cache.removeAllAsync();

      // Luego establecemos en 0 la variable.
      localStorage.setItem(CacheType.CLEAR_CACHE, "1");

      return true;
    }

    return false;
  }

  /**
   * Obtiene la información básica necesaria para el funcionamiento de la App.
   */
  getBaseItems(): void {
    if (this.network.type == "none") {
      this.messagesService.presentToast('No tenés conexión a Internet', 'bottom', true);
      return;
    }

    // Verificamos si es necesario actualizar la información de troncales
    this.cache.isCacheOutdated(CacheType.TRONCALES).then((isOutdated) => {
      if (!isOutdated) return;
      console.log("getBaseItems - TRONCALES desactualizado");
      this.fetchTroncales();
    });

    // Verificamos si es necesario actualizar las configuraciones
    this.cache.isCacheOutdated(CacheType.CONFIGURACIONES).then((isOutdated) => {
      if (!isOutdated) return;
      // obtenemos la info de configuración desde el API
      this.apiService.getInfoApp().subscribe(
        data => { // Success callback
          this.setConfiguracionesInStorage(data[ConfigType.CONFIG_KEY]);
          this.checkVersionVigenteAndToastResult(data);
        },
        console.error); // completed callback
    });
  }

  /**
   * Verifica la versión de la App y muestra las alertas correspondientes.
   * @param data Versión a verificar.
   */
  private checkVersionVigenteAndToastResult(data: any) : Promise<boolean> {
    if (data == null) return Promise.resolve(true);

    return this.appVersion.getVersionNumber().then((currentVersion) => {
        if (!currentVersion) return true;
        const versionMinima = data[ConfigType.CONFIG_VER_MIN];

        if (this.versionCompare(currentVersion, versionMinima) < 0) {
          const mensajeVersionMinima = data[ConfigType.CONFIG_TXT_VER_MIN];
          const alert = this.createInvalidVersionAlert(mensajeVersionMinima);
          alert.present();
          return false;
        }
        const versionRecomendada = data[ConfigType.CONFIG_VER_REC];

        if (this.versionCompare(currentVersion, versionRecomendada) < 0) {
          const mensajeVersionRecomendada = data[ConfigType.CONFIG_TXT_VER_REC];
          const alert = this.createNewestVersionAlert(mensajeVersionRecomendada);
          alert.present();
        }

        return true;
      },
      () => {
        return true;
      }
    );
  }

  private createInvalidVersionAlert(message: string): Alert {
    return this.createVersionAlert(message, true);
  }

  private createNewestVersionAlert(message: string): Alert {
    return this.createVersionAlert(message, false);
  }

  private createVersionAlert(message: string, forceExitApp: boolean): Alert {
    let buttons = [];

    if (this.platform.is('android')) {
      buttons.push({
        text: 'Actualizar',
        handler: () => {
          window.open('market://details?id=ar.gob.cordoba.gobiernoabierto.go', '_system', 'location=yes');
          this.platform.exitApp();
        }
      });
    }

    if (!forceExitApp) {
      buttons.push({
        text: 'Omitir',
        handler: () => {}
      });
    }

    return this.alertCtrl.create({
      title: 'Nueva versión',
      message: message,
      enableBackdropDismiss: !forceExitApp,
      buttons: buttons
    });
  }

  /**
   * Almacena la configuración de la App en memoria interna.
   * @param configuraciones Array de configuraciones a almacenar.
   */
  private setConfiguracionesInStorage(configuraciones: any) {
    let configList = [];

    // recorremos todas las configuraciones recibidas. 
    for (let i = 0; i < configuraciones.length; i++) {
      configList.push({
        Key: configuraciones[i].nombre,
        Value: configuraciones[i].valor 
      });
    }

    this.cache.setItemAsync(CacheType.CONFIGURACIONES, JSON.stringify(configList));
  }

  /** Obtiene el listado de troncales desde el API y almacena la información en memoria interna. */
  private fetchTroncales() {
    this.apiService.getTroncales().subscribe(
      restItems => {
        this.restItems = restItems;
        this.cache.setItemAsync(CacheType.TRONCALES, JSON.stringify(this.restItems.results));
      }
    );
  }

  private versionCompare(vA: string, vB: string) {
    const vAparts = vA.split('.');
    const vBparts = vB.split('.');

    for (let i = 0; i < vAparts.length; ++i) {
      if (vBparts.length == i) return 1;

      const v1part = Number(vAparts[i]);
      const v2part = Number(vBparts[i]);

      if (v1part == v2part) continue;
      if (v1part > v2part) return 1;

      return -1;
    }

    if (vAparts.length != vBparts.length) return -1;

    return 0;
  }

  uid6d() { return Math.floor((1 + Math.random()) * 0x1000000).toString(16).substring(1); }   
  getCurrentTime() { return new Date().getTime(); }
}
