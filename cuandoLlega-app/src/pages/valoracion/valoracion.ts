import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HomePage } from './../home/home';
import { AppVersion } from '@ionic-native/app-version/ngx';
import { AuthServiceProvider } from './../../providers/auth-service/auth-service';
/**
 * Generated class for the ValoracionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-valoracion',
  templateUrl: 'valoracion.html',
})
export class ValoracionPage {
  val: string = "home";

  public excelente :string = "";
  public muybueno : string = "";
  public regular : string = "";
  public malo : string = "";
  public enviado = {"bool":false, "mensaje":"Gracias por completar la valoración, para poder seguir mejorando :D"};
  versionNumber : string = "0.1";
  boolPadd : number = 0;

  constructor(public navCtrl: NavController, public navParams: NavParams, private appVersion: AppVersion,
              private authService:AuthServiceProvider) {
  }

  btnClick(){
    this.boolPadd=1;
  }
  incialBtn(){
    this.boolPadd=0;
  }


  ionViewDidLoad() {
    this.appVersion.getVersionNumber().then(version => {
        this.versionNumber = version;
      });
  }

  volverHome(){
    this.navCtrl.setRoot(HomePage);
  }

  valoracion(tipo){
    
    if(this.excelente !=  ""){ this.authService.postValoracion(tipo,this.excelente,this.versionNumber);this.excelente ="";this.enviado.bool=true}
    if(this.muybueno !=  ""){ this.authService.postValoracion(tipo,this.muybueno,this.versionNumber);this.muybueno="";this.enviado.bool=true}
    if(this.regular !=  ""){ this.authService.postValoracion(tipo,this.regular,this.versionNumber);this.regular="";this.enviado.bool=true}
    if(this.malo !=  ""){ this.authService.postValoracion(tipo,this.malo,this.versionNumber);this.malo="";this.enviado.bool=true}
    this.boolPadd=0;
  }

}
