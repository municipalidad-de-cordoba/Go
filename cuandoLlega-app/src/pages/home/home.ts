import { FavoritosPage } from './../favoritos/favoritos';
import { RecorridosServiceProvider } from './../../providers/recorridos-service/recorridos-service';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, App, ToastController, Events, MenuController } from 'ionic-angular';
import { TapticEngine } from '@ionic-native/taptic-engine/ngx';
import { ApiServiceProvider } from './../../providers/api-service/api-service';
import { AlertController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AppVersion } from '@ionic-native/app-version/ngx';
import {
  GoogleMap,
  GoogleMapOptions,
  Marker,
  MarkerIcon,
  GoogleMapsEvent,
  Polyline,
  PolylineOptions,
  Polygon,
  ILatLng
} from '@ionic-native/google-maps';

import { Poste, Troncal, Linea, Recorrido, Favorito, Parada, RecGeometry, Coordinates } from './../../app/globals';
import { TrackingPage } from '../tracking/tracking';
import { LoadingController } from 'ionic-angular';
import { Platform } from 'ionic-angular';
import { AuthServiceProvider } from './../../providers/auth-service/auth-service';
import { EstadoServicioServiceProvider } from "../../providers/estado-servicio-service/estado-servicio-service";
import { UserInboxServiceProvider } from "./../../providers/user-inbox-service/user-inbox-service";

import { ActionSheetController } from 'ionic-angular';
import { Subscription, timer } from "rxjs";
import "rxjs/add/observable/interval";
import { LoggerServiceProvider } from '../../providers/logger-service/logger-service';

import { CacheType } from "../../providers/cache-service/cache-enum";
import { CacheProvider } from '../../providers/cache-service/cache-service';
import { MapProvider } from '../../providers/map-service/map-service';
import { TTSProvider } from '../../providers/tts-service/tts-provider';

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {
  esperarPag = 'tracking';
  paramsParaSegPagEspera :any  = {
    'parametros' :"",
    'colectivo':"",
    'descripcion':"",
    'recorrido':"",
    'parada':"",
    'geometry': RecGeometry
  };
  public txtLinea : string = "x" ;
  public datosEspera =[];

  versionNumber : string; // Es preferible que sólo diga BETA antes de mostrar una versión incorrecta

  private map: GoogleMap;

  public userDetails:any;

  public listRecorridos;

  public colecBG = false;
  public lineaBG = false;

  public selectedTroncales = []; //eliminar
  public selectedTroncal:Troncal = null;
  public selectedLinea = -1;
  public selectedRecorrido = null;

  public selectedParadaRecorridoTime = null;
  public selectedParadaRecorridoDist = null;

  loader:any;

  polys:Polyline[] =[];
  public troncales : Troncal[] = [];

  favoritos : Favorito [] = [];

  curLat = -31.4201;
  curLng = -64.1888;

  auxMarker : Marker = null;

  postesMarkers : Poste [] = [];
  postesMarkerList: Marker[] = [];
  selectedPoste : Poste;
  selectedParadas : number[] = [];

  auxLineas : Linea [] = [];
  troncs : any;
  curRecorridos : Recorrido[] = [];

  overflow : boolean;

  posteIncioFin = [];

  tutorial : number;

  auxContador = 0;

  auxOpcionesData:any;

  cantidadUnidades: number;

  private estadoServicioSuscription: Subscription;

  // #239_config-api: Establecemos el numero maximo de colectivos en espera
  private maxColectivosEspera :number = 3;

  // #135_notif-usuarios: Cantidad de mensajes sin leer que se mostrarán en la página principal.
  _unreadMsgCount:number = 0;

  private _inboxMsgCountEventHandler:any;

  private _tutorialEventHandler:any;

  /** Indica el estado de la alerta de "Salir de Go" */
  private _alertVisible = false;
  /** Listado de paradas obtenidas desde cache. */
  private listParadas = [];

  private _toast;

  /** Contador de segundos pasados desde que se inició una carga. */
  public loaderCounter = 0;
  /** Timer que se dispara cuando se inicia una carga. */
  private loaderTimer = null;

  private subscription_general: Subscription[] = [];
  private subscription_markers: Subscription[] = [];
  private subscription_cameraMove: Subscription[] = [];

  _loadingLinea: boolean = false;

  private _wifiPoly: Polygon[] = [];
  private wifiMarker: Marker;

  isMapReady = false;

  constructor(public navCtrl: NavController, public navParams: NavParams,  public app:App,
    private toastCtrl: ToastController, private events:Events, private menuCtrl: MenuController,
    private taptic: TapticEngine, private apiService:ApiServiceProvider, public alertCtrl: AlertController,
    private statusBar:StatusBar, private recorridosService : RecorridosServiceProvider,
    private loadingCtrl: LoadingController, private appVersion: AppVersion, public platform: Platform,
    private authService:AuthServiceProvider, private paradaActionSheet: ActionSheetController,
    private estadoServicioService: EstadoServicioServiceProvider, private inboxService: UserInboxServiceProvider, 
    private logs:LoggerServiceProvider, private cache:CacheProvider, private mapProvider: MapProvider, 
    private tts:TTSProvider) {

    platform.ready().then(() => {
      platform.registerBackButtonAction(() => {
        if (this.menuCtrl.isOpen()) {
          this.menuCtrl.close();
        }
        else {
          if (this._alertVisible == false) {
            const confirmAlert = this.alertCtrl.create({
              title: ' ¿Usted desea salir de Go?',
              buttons: [
                {
                  text: 'No',
                  handler: () => {}
                },
                {
                  text: 'Si',
                  handler: () => {
                    platform.exitApp();
                  }
                }
              ]
            });
            this._alertVisible = true;
            confirmAlert.present();
            
            // Actualizamos el estado de ventana una vez que la misma se cierra.
            confirmAlert.onDidDismiss(() => { this._alertVisible = false; });
          }
        }
      },1);

      this.estadoServicioSuscription = this.estadoServicioService.getEstadoServicio()
        .subscribe(value => this.cantidadUnidades = value);

      this.getMinZoom().then((value) => {
        this._minZoom = value;
      });
    });
  }
  
  ionViewCanEnter() {
    this.cache.getItemAsync(CacheType.USER_DATA).then((tmpData) => {
      let data = JSON.parse(tmpData);
      if (data != null && (data.key)) {
        this.cache.getItemAsync(CacheType.TUTORIAL).then((appData) => {
          if (appData != null) this.tutorial = appData;
          else {
            this.tutorial = 1;
            this.cache.setItemAsync(CacheType.TUTORIAL, JSON.stringify(this.tutorial));
          }
        });
        return true; 
      }
      return false;
    });
  }

  ionViewWillEnter() {
    console.log("ionViewWillEnter");

    this.statusBar.styleLightContent();
    this.selectedLinea = -1;
    this.selectedParadas = [];
    this.selectedPoste = null;
    this.selectedRecorrido = null;
    this.selectedTroncal = null;
    this.selectedTroncales = [];
  }

  ionViewDidLoad() {
    console.log("[Home] - ionViewDidLoad");

    // Se establece el texto del header de la App según los valores leidos desde el API
    this.readConfig();

    // Obtenemos la cantidad de mensajes sin leer del usuario.
    this._inboxMsgCountEventHandler = () => {
      console.log("home.ts - inbox:msgCount");
      this._unreadMsgCount = this.inboxService.getUnreadMessagesCount();
    }

    this._tutorialEventHandler = (tutorialVal) => {
      this.logs.dbLog("[HomePage] - Tutorial Event Fired");
      this.tutorial = tutorialVal;
    }

    this.events.subscribe("tutorialEvent", this._tutorialEventHandler);

    // Suscribimos al evento inbox:msgCount para escuchar cualquier cambio al contador de mensajes.
    this.events.subscribe("inbox:msgCount", this._inboxMsgCountEventHandler);

    this.getTroncales();

    // Verificamos si el usuario tiene mensajes nuevos
    this.inboxService.getMessagesFromAPI();
  }

  ionViewDidEnter() { 
    console.log("[Home] - ionViewDidEnter");
 
    //this.presentLoader("Cargando mapa...");
    this.initMap();
  }

  ionViewWillLeave() {
    console.log("ionViewWillLeave");
    if (this._inboxMsgCountEventHandler) {
      // Hay que dejar de escuchar eventos de "inbox:msgCount" para evitar duplicaciones.
      this.events.unsubscribe("inbox:msgCount", this._inboxMsgCountEventHandler);
      this._inboxMsgCountEventHandler = undefined;
    }

    if (this._tutorialEventHandler) {
      this.events.unsubscribe("tutorialEvent", this._tutorialEventHandler);
      this._tutorialEventHandler = undefined;
    }

    this.estadoServicioSuscription.unsubscribe();
    this.estadoServicioSuscription = undefined;

    this.clearMap();

    this.unsubscribeObservables(this.subscription_general);
    this.unsubscribeObservables(this.subscription_markers);
    this.unsubscribeObservables(this.subscription_cameraMove);

    this.map.setPadding(0);

    console.log("ionViewWillLeave - DONE");
  }

  /**
   * Obtiene y establece los valores de configuración de la App.
   */
  readConfig() {
    this.logs.dbLog("(!) Leyendo configuración...");

    this.setVersionFromConfig();

    this.setMaxColectivos();
  }

  /** Establece la cantidad máxima de colectivos que pueden añadirse para seguimiento. */
  private setMaxColectivos() {
    // Cantidad máxima de colectivos a trackear según los valores obtenidos desde el API.
    this.cache.getAppConfig_byKeyAsync(CacheType.CONFIG_MAX_BONDI_ESPERA).then((maxBondis) => {
      this.maxColectivosEspera = parseInt(maxBondis, 10);
      this.logs.dbLog("(**) Max. Colectivos: " + this.maxColectivosEspera);
    });
  }

  /** Establece la versión de la App según el valor leido desde la configuración. */
  private setVersionFromConfig(): void {
    this.cache.getAppConfig_byKeyAsync(CacheType.CONFIG_TXT_APP_HEADER).then((versionFromAPI) => {
      if (versionFromAPI != null) {
        // verificamos si el texto contiene placeholder a ser reemplazado.
        
        // Nota: El RegExp va a buscar por el patron exacto "{{ version }}". Tal cual fue presentado en el ticket #239
        // La idea de esto es dejar programado el reemplazo de {{ version }} por la versión leida desde el config.xml
        // Esto seguirá requiriendo una compilación de la App a la hora de querer mostrar una nueva versión. Sin embargo
        // cuando el API NO presente el patrón especificado, se leera la versión directamente desde internal Storage.
        let re = new RegExp("{{ version }}", "ig");
        if (re.test(versionFromAPI)) {
          this.appVersion.getVersionNumber().then(versionFromFile => {
            this.versionNumber = versionFromAPI.replace(re, versionFromFile);
            this.logs.dbLog("(**) Version: " + this.versionNumber);
          });
        }
        else this.versionNumber = versionFromAPI;
      }
    });
  }

  abrirEsperarPag() {
    this.navCtrl.setRoot(TrackingPage, this.paramsParaSegPagEspera);
  }

  /** Muestra la cantidad de unidades en servicio. */
  showCantidadUnidadesMessage() {
    this.alertCtrl
      .create({message: (this.cantidadUnidades !== undefined) 
        ? 'Hay ' + this.cantidadUnidades + ' unidades en servicio en este momento' 
        : 'Obteniendo cantidad de unidades en servicio...'})
      .present();
  }

  /** Muestra u oculta la lista de troncales. */
  toggleTroncales() {
    this.selectedTroncal = null;
    this.selectedLinea = -1;
    this.colecBG = !this.colecBG;
    var curButton = document.getElementById("btn_troncales");

    if (this.colecBG) {
      var ramalesDiv = document.getElementById("ramales");
      ramalesDiv.scrollTop = ramalesDiv.scrollHeight;
      this.overflow = this.checkOverflowHeight("ramales");

      this.tts.speak("Listado de troncales desplegado.");

    }
    else {
      this.selectedTroncales = [];
      curButton.blur(); // Limpiamos el foco del botón seleccionado.

      this.tts.speak("Listado de troncales oculto.");
    }
  }

  updateUnidadesActivas() {
    if (!this.selectedTroncal) return;
    this.apiService.getUnidadesActivas(this.selectedTroncal.id).subscribe((response) => {
      console.log("unidades activas obtenidas: ", response);
      if (response != null) {
        // Debido a la naturaleza de los requests a veces se intenta ejecutar esta parte del código
        // cuando el usuario ya se encuentra en otro lugar. Debemos re-verificar que selectedTroncal
        // tenga un valor adecuado.
        if (!this.selectedTroncal) return;
        let data = response["results"];

        // recorremos todas las lineas del troncal seleccionado
        for (let i = 0; i < this.selectedTroncal.lineas.length; i++) {
          if (this.selectedTroncal.lineas[i] == null) continue;
          let curLinea = this.selectedTroncal.lineas[i];
          
          // recorremos ahora las lineas recibidas desde unidades-activas
          for (let j = 0; j < data.length; j++) {
            if (data[j] == null) continue;

            if (parseInt(curLinea.id,10) === data[j].id) {
              curLinea.unidadesActivas = data[j].activos;
            }
          }
        }
      }
    });
  }

  selectTroncal(t) {
    this.curRecorridos = null;
    if (this.selectedTroncal == t) {
        this.selectedTroncal = null;
        this.selectedLinea = -1;
    }
    else {
      this.selectedTroncal = t;

      this.tts.speak("Menú de líneas del Troncal " + this.selectedTroncal.nombre + " desplegado hacia la derecha.");

      this.updateUnidadesActivas();

      let overflowSub = timer(100).subscribe(() => {
        t.overflow = this.checkOverflow("lineas_"+ t.id);

        overflowSub.unsubscribe();
      });
    }
  }

  /** Inicializa un timer de carga. */
  initLoadingTimer() {
    this.loaderTimer = setInterval(() => {
      this.loaderCounter++;

      if (this.loaderCounter >= 10) {
        this.stopLoadingTimer();

        this.alertCtrl.create({
          message: "La obtención de datos está tardando mas de lo esperado. La información seleccionada se actualizará pronto.",
          buttons: ["Aceptar"]
        }).present();
      }
    }, 1000);
  }

  /** Finaliza el timer de carga. */
  stopLoadingTimer() {
    // Limpiamos el "timer"
    clearInterval(this.loaderTimer);
    // Ocultar Buscando Info
    this.loader.dismiss();
    // Reiniciamos el contador
    this.loaderCounter = 0;
  }

  selectLinea(l) {
    this.txtLinea = l.nombre;
    this.auxContador = 0;
    //this.logs.dbLog("selectLinea: " + JSON.stringify(l));

    if (this.selectedLinea == l) this.selectedLinea = -1;
    else
    {
      this.selectedLinea = l;
      this.curRecorridos = null;
      let results: any;
      let auxCoords : Coordinates[] = [];

      // Mostrar Buscando Info
      this.presentLoader('Obteniendo datos...');
      // Iniciamos el timer de carga.
      this.initLoadingTimer();

      this.subscription_general.push(this.recorridosService.getRecorridos(l.nombre).subscribe(restItems => {
        if (restItems.length > 0) { this.curRecorridos = restItems; }
        else {
          let listRecorridos:Recorrido[] = [];
          results = restItems;
          results.results.features.forEach(rec => {
            let auxLinea = new Linea(rec.properties.linea.id, rec.properties.linea.id_externo, null, rec.properties.linea.nombre_publico, 
              rec.properties.linea.empresa, rec.properties.linea.id_empresa);
            auxCoords = [];
            rec.geometry.coordinates.forEach(geo => { auxCoords.push(new Coordinates(geo[1], geo[0])); });
            let auxGeo = new RecGeometry(rec.geometry.type, auxCoords);
            let auxRecorrido = new Recorrido(rec.id, rec.properties.id_externo, auxLinea, rec.properties.descripcion_corta, rec.properties.descripcion_cuando_llega, auxGeo);
            listRecorridos.push(auxRecorrido);
            this.recorridosService.setRecorrido(auxRecorrido);
          });
          this.curRecorridos = listRecorridos;
        }

        // Finalizamos con el timer.
        this.stopLoadingTimer();

        this.tts.speak("Listado de recorridos de la línea " + this.txtLinea + " obtenido.");
      },
      err => {
        this.stopLoadingTimer();
        
        this.presentToast("No se ha podido establecer conexión con los servidores. Estamos trabajando para solucionarlo.");
        this.logs.dbErr(JSON.stringify(err));
      }));
    }
  }

  mostrarParadasLinea(rec) {
    this.selectedTroncal = null;
    this.colecBG = false;
    this.removePostes();
    this.removePolys();

    this._loadingLinea = true;
    this.selectedRecorrido = this.recorridosService.getRecorrido(rec);

    this.drawRecorrido(this.selectedRecorrido);

    //Obtener paradas solo para un recorrido + mostrar recorrido y paradas en el mapa.
    this.getPostesR(this.map, this.selectedRecorrido);
  }

  /**
   * Muestra un mensaje informativo en la pantalla.
   * @param mensaje Mensaje a mostrar.
   */
  presentToast(mensaje) {
    // Despachamos cualquier alerta que haya habido previamente.
    try {
      this._toast.dismiss();
    } catch(e) {}

    this._toast = this.toastCtrl.create({
      message: mensaje,
      duration: 3000,
      position: 'top'
    });

    this._toast.present();
  }

  presentLoader(mensaje){
    this.loader = this.loadingCtrl.create({
        content: mensaje
    });
    this.loader.present();
  }

  initMap() {
    this.map = this.mapProvider.getInstance("map");

    this.map.one(GoogleMapsEvent.MAP_READY).then(() => {
      this.logs.dbLog("(**) GOOGLE MAPS READY");
      this.mostrarZonaWifi(); //dibuja el poligono de las zonas Wifi
    
      this.map.getMyLocation().then(result => {
        this.curLat = result.latLng.lat;
        this.curLng = result.latLng.lng;
  
        setTimeout(() => {
          // -- BUG POINT!!!
          this.map.animateCamera({
            target: {lat: this.curLat, lng: this.curLng},
            zoom: 15,
            duration: 800
          }).then(() => this.isMapReady = true);
          // -- BUG POINT
        }, 350);
  
        this.map.setMyLocationEnabled(true);
        this.map.setMyLocationButtonEnabled(true);
        this.map.setPadding(160, 15, 0, 0); 
      }).catch(error => {
        this.logs.dbErr("Error al obtener ubicación: " + JSON.stringify(error));
  
        setTimeout(() => {
          // -- BUG POINT!!!
          this.map.animateCamera({
            target: {lat: this.curLat, lng: this.curLng},
            zoom: 15,
            duration: 800
          }).then(() => this.isMapReady = true);
          // -- BUG POINT
        }, 350);
  
        this.map.setMyLocationEnabled(false);
        this.map.setMyLocationButtonEnabled(false);
        this.map.setPadding(0);
      }).catch(error => {
        console.log("entro al error")
        console.log(error);
      });
    });
  }

  /** Obtiene la información de troncales desde memoria interna. */
  getTroncales() {
    this.cache.getItemAsync(CacheType.TRONCALES).then((tmpData) => {
      let data = JSON.parse(tmpData);
      if (data == null) {
        this.logs.dbErr("La información de troncales en cache no existe");
        return;
      }
      for (let i = 0; i < data.length; i++) {
        if (data[i] === null) continue;
        let troncal = data[i];
        let tmpLineas = [];

        for (let j = 0; j < troncal.lineas.length; j++) {
          if (troncal.lineas[j] === null) continue;
          let l = troncal.lineas[j];
          tmpLineas.push(new Linea(l.id, l.id_externo, troncal.id, l.nombre_publico, l.empresa, l.id_empresa));
        }

        this.troncales.push(new Troncal(troncal.id, troncal.nombre_publico, tmpLineas));
      }
    });
  }

  mostrarZonaWifi() {
    let results : any;
    
    this.cache.isCacheOutdated(CacheType.ZONAS_WIFI).then((outdated) => {
      if (!outdated) {
        this.cache.getItemAsync(CacheType.ZONAS_WIFI).then((results) => {
          this.setWifiPoints(JSON.parse(results));
        });
      }
      else {
        this.logs.dbLog("Solicitando zonas WiFi al servidor...");

        this.apiService.getZonasWifi().subscribe(
          data => { results = data; },
          err => { console.error(err); },
          () => { 
            this.setWifiPoints(results); 
            this.cache.setItemAsync(CacheType.ZONAS_WIFI, JSON.stringify(results));
          }
        );
      }
    });
  }

  private setWifiPoints(results) {
    if (results.results.features[0] == null) return;
    if (results.results.features[0].geometry.coordinates[0] == null) return;
    
    let POINTS: ILatLng[] = [];
    
    let pointCoords = results.results.features[0].geometry.coordinates[0];
    
    for (let i = 0; i < pointCoords.length; i++) {
      POINTS.push({lat: pointCoords[i][1], lng: pointCoords[i][0] });
    }

    let options: PolylineOptions = {
      'points': POINTS,
      'strokeColor' : 'rgb(0, 255, 170)',
      'fillColor' : 'rgb(0, 255, 170)',
      'strokeWidth': 10
    };

    this.map.addPolygon(options).then((polygon: Polygon) => {
        polygon.setClickable(true);
        polygon.getPoints();

        this._wifiPoly.push(polygon);
    });
    
    let wifiIcon: MarkerIcon = {
      url: './assets/imgs/wifi-gratis-201-150.png',
      size: {
        width: 80,
        height: 80
      }
    };
    this.map.addMarker({
      position: POINTS[2],
      icon:wifiIcon
    }).then((marker: Marker) => {
        //htmlInfoWindow.open(marker);
        this.wifiMarker = marker;
    });
  }

  drawRecorrido(rec:Recorrido) {
    this.removePosteInicioFin();
    let paradaInicio: MarkerIcon = {
        url: './assets/imgs/inicio.png',
        size: {
          width: 60,
          height: 60
        }
    };
    let paradaFin: MarkerIcon = {
        url: './assets/imgs/fin.png',
        size: {
          width: 60,
          height: 60
        }
    };
    this.map.addMarker({
      position: {lat: rec.geometry.coordinates[0].lat, lng: rec.geometry.coordinates[0].lng},
      icon: paradaInicio
    }).then((marker: Marker) =>{
      let auxPoste = marker;
      if (this.posteIncioFin.indexOf(auxPoste)==-1) {
        this.posteIncioFin.push(auxPoste);
      }
    });

    this.map.addMarker({
      position: {lat: rec.geometry.coordinates[rec.geometry.coordinates.length - 1].lat, lng: rec.geometry.coordinates[rec.geometry.coordinates.length - 1].lng},
      icon: paradaFin
    }).then((marker: Marker) =>{
      let auxPoste = marker;
      if (this.posteIncioFin.indexOf(auxPoste)==-1) {
        this.posteIncioFin.push(auxPoste);
      }
    });

    this.map.addPolyline({
      points: rec.geometry.coordinates,
      'color': '#6f42c1',//this.getRandomColor(),
      'width': 4,
      'geodesic': true
    }).then((polyline: Polyline) => {
      if(this.polys.indexOf(polyline)==-1){
          this.polys.push(polyline);
      }
    });
  }

  removePosteInicioFin() {
    for (let i = 0; i < this.posteIncioFin.length; i++) {
      if (this.posteIncioFin[i]) this.posteIncioFin[i].remove();
    }

    this.posteIncioFin = [];

    // Borramos también los marcadores de wifi
    if (this.wifiMarker) {
      this.wifiMarker.remove();
      this.wifiMarker = undefined;
    }
  }

  getRandomColor()
  {
      var color = "#";
      for (var i = 0; i < 3; i++)
      {
          var part = Math.round(Math.random() * 255).toString(16);
          color += (part.length > 1) ? part : "0" + part;
      }
      return color;
  }

  /**
   * Obtiene los postes pertenecientes a un Recorrido
   * @param map Instancia de Google Maps
   * @param rec Recorrido seleccionado.
   */
  getPostesR(map:GoogleMap, rec:Recorrido) {
    let boundStr = map.getVisibleRegion().northeast.lng +","+ map.getVisibleRegion().northeast.lat +","+map.getVisibleRegion().southwest.lng +","+map.getVisibleRegion().southwest.lat;

    console.log("Home - getPostesR");

    this.getPostesMarker(map);
    this.unsubscribeObservables(this.subscription_cameraMove);

    // Verificamos la caducidad del cache.
    this.cache.isCacheOutdated(CacheType.POSTES_RECORRIDO).then((outdated) => {
      if (outdated) this.getPostesData(rec, boundStr);
      else {
        // Obtenemos la info desde cache.
        this.cache.getItemAsync(CacheType.POSTES_RECORRIDO).then((tmpPostes) => {
          if (tmpPostes) {
            let data = JSON.parse(tmpPostes);
            let curPoste = this.getPostesDataByRecID(data, parseInt(rec.id, 10));

            for (let i = 0; i < data.length; i++) this.addParadasToCacheList(data[i].RecID, data[i].Value);

            if (!curPoste) this.getPostesData(rec, boundStr);
            else 
            {
              this.setPostesData(JSON.parse(curPoste.Value), rec);

              this.subscription_cameraMove.push(this.map.on(GoogleMapsEvent.CAMERA_MOVE_END).subscribe(() => {
                if (this.selectedRecorrido != null) {
                  console.log("(!) [getPostesR] - CAMERA_MOVE_END");
                  if (curPoste) this.checkMapZoom(JSON.parse(curPoste.Value), this.selectedRecorrido);
                }
              }));
            }
          }
        }); 
      }
    });
  }

  /**
   * Busca información de un poste basado en el ID del recorrido
   * @param postesData Listado de todos los postes disponibles en memoria.
   * @param recID ID del recorrido del cual seleccionar sus postes.
   */
  getPostesDataByRecID(postesList: any, recID: number) {
    let data = null;
    if (postesList == null) return data;

    for (let i = 0; i < postesList.length; i++) {
      let curPoste = postesList[i];

      if (curPoste.RecID == recID) {
        data = curPoste;
        break;
      }
    }

    return data;
  }

  /**
   * Obtiene información desde el API de los postes de un recorrido seleccionado.
   * @param rec Recorrido seleccionado.
   * @param boundStr Boundaries
   */
  getPostesData(rec: Recorrido, boundStr: string) {
    let results : any;

    this.logs.dbLog("Solicitando Postes al servidor...");
    this.apiService.getPostesRecorrido(rec.id, boundStr).subscribe(
      data => { results = data},
      err => {
        this.logs.dbErr("Error: " + err);
        this.presentToast("Ha ocurrido un error, intenta nuevamente. Si el error persiste, contactanos!");
      },() => { 
        this.setPostesData(results, rec);
        this.addParadasToCacheList(rec.id, JSON.stringify(results));
        this.cache.setItemAsync(CacheType.POSTES_RECORRIDO, JSON.stringify(this.listParadas));

        this.subscription_cameraMove.push(this.map.on(GoogleMapsEvent.CAMERA_MOVE_END).subscribe(() => {
          if (this.selectedRecorrido != null) {
            console.log("(!) [PostesData] CAMERA_MOVE_END");
            this.checkMapZoom(results, this.selectedRecorrido);
          }
        }));
    });
  }

  /**
   * Establece la información de los marcadores de Paradas en el Mapa
   * @param results Listado de postes de un recorrido seleccionado.
   * @param rec Recorrido seleccionado.
   */
  setPostesData(results: any, rec: Recorrido)
  {
    //if (results.count == 0) {
      // Ubicamos la camara aproximadamente en la mitad del recorrido.
      let auxLength = Math.trunc(rec.geometry.coordinates.length / 2);
      if (this.auxContador < 1) {
        this.map.animateCamera({
          target: {lat: rec.geometry.coordinates[auxLength].lat, lng: rec.geometry.coordinates[auxLength].lng},
          duration: 800,
          zoom: this._minZoom
        }).then(() => {});

        this._loadingLinea = false;
      }
      this.auxContador = this.auxContador + 1;
    //}

    this.checkMapZoom(results, rec);
  }

  checkMapZoom(results, rec) {
    let paradaIcon: MarkerIcon = {
      url: './assets/imgs/map-marker-point.png',
      size: {
        width: 50,
        height: 50
      }
    };

    // Mostramos / ocultamos las paradas dependiendo el zoom de la camara
    if (this.map.getCameraZoom() >= this._minZoom) {
      results.results.features.forEach(p => {
        this.map.addMarker({
            position: {lat: p.geometry.coordinates[1], lng: p.geometry.coordinates[0]},
            icon: paradaIcon
        }).then((marker: Marker) => {
          let auxPoste = new Poste(p.id, p.properties.id_externo, p.properties.descripcion, p.properties.sentido, p.properties.calle_principal, p.properties.calle_secundaria, marker)
          if (this.postesMarkers.indexOf(auxPoste) == -1) {
            this.postesMarkers.push(auxPoste);
          }

          //this.postesMarkerList.push(marker);
          
          // Bindeamos un evento de click sobre un Marcador de Parada
          this.subscription_markers.push(marker.on(GoogleMapsEvent.MARKER_CLICK).subscribe(() => {
            this.taptic.impact({ style: "light" });

            // this.presentParadaActionSheet(rec, auxPoste);
            this.setTracking(rec, auxPoste);
          }));
        });
      });
    }
    else this.removePostes();
  }

  /**
   * Establece los postes correspondientes a un recorrido seleccionado. 
   * @param map Instancia de Google Maps
   */
  getPostesMarker(map:GoogleMap)
  {
    if (this.postesMarkers.length > 0) {
      let filteredPostes : any = [];

      for (let i = 0; i <= this.postesMarkers.length; i++) {
        if (!this.postesMarkers[i]) continue;
        let p = this.postesMarkers[i];

        if (p.marker.getPosition().lat < map.getVisibleRegion().southwest.lat || p.marker.getPosition().lat > map.getVisibleRegion().northeast.lat ||
            p.marker.getPosition().lng < map.getVisibleRegion().southwest.lng || p.marker.getPosition().lng > map.getVisibleRegion().northeast.lng) {
            p.marker.remove();
        }
        else filteredPostes.push(p);
      }
      this.postesMarkers = filteredPostes;
    }
  }

  goToFavorites() {
    this.navCtrl.push(FavoritosPage);
  }

  findParada(paradas:Parada[], idParada:string):Parada {
    let auxParada = null;
    paradas.forEach(function (par) {
      if(par.id == idParada){
        auxParada = par;
      }
    });
    return auxParada;
  }

  removeParadas() {

  }

  removePostes() {
    console.log("REMOVE POSTES");
    for (let i = 0; i < this.postesMarkers.length; i++) {
      if (this.postesMarkers[i] != null && this.postesMarkers[i].marker != null) {
        this.postesMarkers[i].marker.off();
        this.postesMarkers[i].marker.empty();
        this.postesMarkers[i].marker.remove();
        this.postesMarkers[i].marker = undefined;
      }
    }

    this.postesMarkers = [];
    this.unsubscribeObservables(this.subscription_markers);

    /*for (let i = 0; this.postesMarkerList.length; i++) {
      if (this.postesMarkerList[i]) this.postesMarkerList[i].remove();
    }

    this.postesMarkerList = [];*/
  }

  removePolys() {
    for (let i = 0; i < this.polys.length; i++) {
      if (this.polys[i]) this.polys[i].remove();
    }

    for (let i = 0; i < this._wifiPoly.length; i++) {
      if (this._wifiPoly[i]) this._wifiPoly[i].remove();
    }

    this.polys = [];
    this._wifiPoly = [];
  }

  getNav() {
    var navs = this.app.getRootNavs();
    if (navs && navs.length > 0) {
      return navs[0];
    }
    return this.app.getActiveNav();
  }

  checkOverflow (element) {
    let e = document.getElementById(element)
    if (e.offsetHeight < e.scrollHeight ||
        e.offsetWidth < e.scrollWidth) {
      return true;
    } 
    else
      return false;
  }

  checkOverflowHeight (element) {
    let e = document.getElementById(element)
    if (screen.height-150 < e.scrollHeight || //100 viene de la altura del navbar y los botones de abajo
        screen.width < e.scrollWidth) {
      return true;
    } 
    else
      return false;
  }
  pageFavoritos(){
    this.navCtrl.setRoot(FavoritosPage);
  }

  /**
   * Abre la página de seguimiento.
   */
  pageEspera() {
    // Movemos la página en el stack de Views. 
    // Al usar .push() las instancias de Home quedan almacenadas en caché.
    // Esto hace que se comparta un mismo mapa entre las dos vistas
    // en lugar de crear instancias nuevas, lo que aumenta la respuesta
    // considerablemente.
    //this.navCtrl.push(TrackingPage, {}, {animate: false}) ;
    this.navCtrl.setRoot(TrackingPage);
  }

  omitirTutorial() {
    this.tutorial = 0;
    this.cache.setItemAsync(CacheType.TUTORIAL, JSON.stringify(this.tutorial));
  }
  
  presentParadaActionSheet(recorrido:any, selectedPoste:any) {
    let actionSheet = this.paradaActionSheet.create({
      title: '',
      buttons: [
        {
          text: 'Notificar problema en esta parada',
          handler: () => {
            this.presentParadaProblemActionSheet(selectedPoste);
          }
        },
        {
          text: 'Ver cuando llega el colectivo',
          handler: () => {
            this.setTracking(recorrido, selectedPoste);
          }
        },
        {
          text: 'Cancelar',
          role: 'cancel',
          handler: () => {
          }
        }
      ]
    });
 
    actionSheet.present();
  }

  presentParadaProblemActionSheet(parada:any) {
    let alert = this.alertCtrl.create();
    alert.setTitle('¿Qué problema/s encontraste en la parada?');

    alert.addInput({
        type: 'checkbox',
        label: 'Ninguno',
        value: 'ninguno',
        checked: false
      });

    this.cache.getItemAsync(CacheType.CONFIG_NOTIF_NOV_PARADAS).then((data) => {
      this.auxOpcionesData = JSON.parse(data);

      for (let opc in this.auxOpcionesData) {
        var str =  this.auxOpcionesData[opc];
        alert.addInput({
          type: 'checkbox',
          label: str,
          value: opc,
          checked: false
        });
      }
    });
    
    alert.addButton('Cancelar');
    alert.addButton({
      text: 'Enviar',
      handler: data => {
        console.log('Checkbox data:', data);
        this.authService.postNovedadesParadas(JSON.stringify(data), parada.id, this.versionNumber)
        .then(data => {
            console.log(data);
            this.presentToast("Muchas gracias por tu opinión. Estaremos trabajando para mejorar nuestro servicio.");
        })
        .catch(error => {
            console.log(error);
            this.presentToast("No pudimos procesar tu opinión. Por favor, intentá nuevamente en unos minutos.");
        });
      }
    });
    alert.present();
  }

  private _minZoom: number = 14;

  getMinZoom(): Promise<number> {
    return this.cache.getItemAsync(CacheType.CONFIG_MIN_ZOOM).then((tmpData) => {
      let dataRes = 14; 
      if (tmpData) {
        let data = JSON.parse(tmpData);
        dataRes = data;
        return dataRes;
      }
      return dataRes;
    });
  }

  /**
   * Inicia el seguimiento de un colectivo según recorrido y poste.
   * @param recorrido Recorrido al cual hacer seguimiento
   * @param selectedPoste Poste seleccionado.
   */
  setTracking(recorrido:any, selectedPoste:any) {
    let resRecorridoParadas : any;
    this._loadingLinea = true;
    
    this.apiService.getTracking(recorrido.id,selectedPoste.id).subscribe(
      data => resRecorridoParadas = data,
      err => {
        // Se produjo un error no esperado, se notifica al servidor.
        this.authService.postColectivoGrave(this.curLat, this.curLng, selectedPoste.id, recorrido.id, err.status, err.message, this.versionNumber);
        let alert = this.alertCtrl.create();

        alert.setTitle('No se pudo procesar su consulta, disculpe las molestias. Estamos trabajando para solucionarlo.');
        alert.present();
        //this._loadingLinea = false;
      },
      () => {
        // La solicitud fue exitosa, se inicia el seguimiento.
        
          this.paramsParaSegPagEspera.parametros = resRecorridoParadas;
          this.paramsParaSegPagEspera.descripcion = recorrido.descripcion_larga;
          this.paramsParaSegPagEspera.colectivo = recorrido.linea.nombre;
          this.paramsParaSegPagEspera.recorrido = recorrido.id;
          this.paramsParaSegPagEspera.parada = selectedPoste.id;
          this.paramsParaSegPagEspera.geometry = recorrido.geometry;
          this.paramsParaSegPagEspera.changed = 0;
          
          // Se notifica al servidor solo la primera vez que se ha seleccionado el poste.
          //if (selectedPoste.statCodeSent != true) {
            this.logs.dbLog("Enviando notificación al servidor sobre el poste: " + selectedPoste.id + " por primera vez.");
            
            if (resRecorridoParadas[0].error == 0 || resRecorridoParadas[0].error == 3 ) {
              this.paramsParaSegPagEspera.tiempo_inicial = resRecorridoParadas[0].resultados.tiempo_minutos;
              this.paramsParaSegPagEspera.tiempo_anterior = resRecorridoParadas[0].resultados.tiempo_minutos;

              this.authService.postColectivo(this.curLat, this.curLng, selectedPoste.id, recorrido.id, 
                resRecorridoParadas[0].resultados.tiempo_minutos, resRecorridoParadas[0].resultados.distancia_km, this.versionNumber);
            }

              //selectedPoste.statCodeSent = true;
          //}

          //this._loadingLinea = false;
          this.addFicha(this.paramsParaSegPagEspera);
        /*else {
          // La solicitud devolvió un error, se notifica al servidor. 
          this.authService.postColectivoError(this.curLat, this.curLng,selectedPoste.id, recorrido.id, resRecorridoParadas[0].error, resRecorridoParadas[0].detalles, this.versionNumber);
          let alertRP = this.alertCtrl.create({title: resRecorridoParadas[0].detalles});
          alertRP.present();
        }*/
      }
    );
  }

  addFicha(fichaData:any) {
    this.logs.dbLog("(!) Entrando a addFicha. Leyendo Cache LISTA_FICHAS...");
    this.cache.getItemAsync(CacheType.LISTA_FICHAS).then((tmpList) => {
      let list = JSON.parse(tmpList);
      this.logs.dbLog("(*) Lista leida: " + JSON.stringify(list));
      if (list) {
        let existsIndex = this.existsFicha(list, fichaData);
        if (existsIndex == -1) {
          // #239_config-api: leemos el valor directamente desde la configuración
          console.log("[addFicha] - MaxColectivosEspera: " + this.maxColectivosEspera);
          if (list.length >= this.maxColectivosEspera) {
            let alert = this.alertCtrl.create();
            alert.setTitle('Ya seleccionaste ' + this.maxColectivosEspera + ' paradas, si queres cambiar podes hacerlo desde espera.');
            alert.present();
            return;
          }
          else {
            list.unshift(fichaData);
          }
        }
        else {
          // Para que no haya repetidos, y el seleccionado sea el primero en el slider
          list.splice(existsIndex,1);
          list.unshift(fichaData);
        }
      }
      else {
        list = [];
        list.push(fichaData);
      }

      this.cache.setItemAsync(CacheType.LISTA_FICHAS, JSON.stringify(list));

      this.navCtrl.setRoot(TrackingPage);
    });
  }

  existsFicha(list:any, fichaData:any):number{
    let indexObj = -1;

    for (let i = 0; i < list.length; i++) {
      if (list[i] != null) {
        if (list[i].colectivo == fichaData.colectivo && 
            list[i].recorrido == fichaData.recorrido &&
            list[i].parada == fichaData.parada) {
              return i;
        }
      }
    }

    /*list.forEach((ficha, index) => {
      if (ficha.colectivo == fichaData.colectivo && ficha.recorrido  == fichaData.recorrido && ficha.parada  == fichaData.parada){
        indexObj = index;
      }
    });*/

    return indexObj;
  }

  /**
   * Configura y establece las opciones deseadas para el mapa de Google.
   * @returns Devuelve un objeto GoogleMapOptions con las configuraciones deseadas para el mapa de Google.
   */
  setGoogleMapOptions() : GoogleMapOptions {
    console.log("setGoogleMapOptions()");
    let mapOptions: GoogleMapOptions = {
      gestures: {
        scroll: true,
        tilt: false,
        zoom: true,
        rotate: true,
      },
      preferences: {
        zoom: {
          minZoom: 11.5,
          maxZoom: 18
        }
      },

      styles: [
        {
          "featureType": "water",
          "elementType": "geometry",
          "stylers": [
              {
                  "color": "#e9e9e9"
              },
              {
                  "lightness": 17
              }
          ]
        },
        {
          "featureType": "landscape",
          "elementType": "geometry",
          "stylers": [
              {
                  "color": "#f5f5f5"
              },
              {
                  "lightness": 20
              }
          ]
        },
        {
          "featureType": "road.highway",
          "elementType": "geometry.fill",
          "stylers": [
              {
                  "color": "#ffffff"
              },
              {
                  "lightness": 17
              }
          ]
        },
        {
          "featureType": "road.highway",
          "elementType": "geometry.stroke",
          "stylers": [
              {
                  "color": "#ffffff"
              },
              {
                  "lightness": 29
              },
              {
                  "weight": 0.2
              }
          ]
        },
        {
          "featureType": "road.arterial",
          "elementType": "geometry",
          "stylers": [
              {
                  "color": "#ffffff"
              },
              {
                  "lightness": 18
              }
          ]
        },
        {
          "featureType": "road.local",
          "elementType": "geometry",
          "stylers": [
              {
                  "color": "#ffffff"
              },
              {
                  "lightness": 16
              }
          ]
        },
        {
          "featureType": "poi",
          "elementType": "geometry",
          "stylers": [
              {
                  "color": "#f5f5f5"
              },
              {
                  "lightness": 21
              }
          ]
        },
        {
          "featureType": "poi.park",
          "elementType": "geometry",
          "stylers": [
              {
                  "color": "#c0ecae"
              },
              {
                  "lightness": 21
              }
          ]
        },
        {
          "elementType": "labels.text.stroke",
          "stylers": [
              {
                  "visibility": "on"
              },
              {
                  "color": "#ffffff"
              },
              {
                  "lightness": 16
              }
          ]
        },
        {
          "elementType": "labels.text.fill",
          "stylers": [
              {
                  "saturation": 36
              },
              {
                  "color": "#333333"
              },
              {
                  "lightness": 40
              }
          ]
        },
        {
          "elementType": "labels.icon",
          "stylers": [
              {
                  "visibility": "off"
              }
          ]
        },
        {
          "featureType": "transit",
          "elementType": "geometry",
          "stylers": [
              {
                  "color": "#f2f2f2"
              },
              {
                  "lightness": 19
              }
          ]
        },
        {
          "featureType": "administrative",
          "elementType": "geometry.fill",
          "stylers": [
              {
                  "color": "#fefefe"
              },
              {
                  "lightness": 20
              }
          ]
        },
        {
          "featureType": "administrative",
          "elementType": "geometry.stroke",
          "stylers": [
              {
                  "color": "#fefefe"
              },
              {
                  "lightness": 17
              },
              {
                  "weight": 1.2
              }
          ]
        }
    ],
      camera: {
        target: {
          lat: this.curLat,
          lng: this.curLng
        },
        zoom: 14
      },
    };

    return mapOptions;
  }

  /**
   * Añade una parada al listado de cache.
   * @param recID ID del recorrido al cual pertenece la parada.
   * @param value Datos del recorrido a almacenar.
   */
  addParadasToCacheList(recID, value: string) {
    if (!this.existsParadaOnCache(recID)) {
      this.listParadas.push({
        RecID: recID,
        Value: value
      });
    }
  }

  /**
   * Verifica si una parada ya fue añadida al cache.
   * @param recID ID del recorrido a buscar.
   */
  existsParadaOnCache(recID) {
    for (let i = 0; i < this.listParadas.length; i++) {
      if (this.listParadas[i].RecID == recID) return true;
    }
    return false;
  }

  unsubscribeObservables(subList: Subscription[]) {
    for (let i = 0; i < subList.length; i++) {
      subList[i].unsubscribe();
      subList[i] = undefined;
      subList.splice(i, 1);
    }
  }

  clearMap() {
    this.removePosteInicioFin();
    this.removePostes();
    this.removePolys();
  }
}
