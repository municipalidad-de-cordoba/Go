import { Component } from '@angular/core';
import { NavParams, NavController, ToastController } from 'ionic-angular';
import { UserInboxServiceProvider } from "../../providers/user-inbox-service/user-inbox-service";
import { UserMessage } from '../../app/globals';

@Component({
  selector: 'page-user-inbox',
  templateUrl: 'user-inbox-msg.html',
})

/**
 * @description Representa la página con los detalles de un mensaje que el usuario ha seleccionado
 */
export class UserInboxMsgPage {
  /** Mensaje actual que el usuario está leyendo */
  curMsg:UserMessage;

  /** Indica si el usuario salió de la pantalla del mensaje sin reaccionar al mismo. */
  private userReacted:boolean = false;

  constructor(params: NavParams, private inboxService:UserInboxServiceProvider, private navCtrl: NavController, private toastCtrl: ToastController) {
    this.curMsg = params.data.item;
  }

  ionViewWillLeave() {
    // Si el usuario abandonó la pantalla sin haber reaccionado a un comentario lo notificamos al API
    if (!this.userReacted) this.setMessageReaction(200, this.curMsg.id);
  }

  /**
   * Establece la reacción que tuvo un usuario a un mensaje.
   * @param codeID Código de reacción
   * @param msgID ID del mensaje al cual el usuario reacciona
   */
  public setMessageReaction(codeID:number, msgID:number) {
    let toastMsg = "";
    let msgDeleted = false;

    switch (codeID) {
      case 200: // Leido
        msgDeleted = false;
        break;

      case 300: // Me Gusta
      case 400: // No me gusta
        this.userReacted = true;
        msgDeleted = true;
        toastMsg = "Gracias por su opinión!";
        break;

      case 500: // Eliminado
        this.userReacted = true;
        msgDeleted = true;       
        toastMsg = "El mensaje ha sido eliminado!";
        break;
    }

    // Enviamos la reacción al API
    this.inboxService.sendMessageReaction(codeID, msgID);

    // Se llama a navCtrl.pop() Solamente si la reacción incluye eliminar el mensaje
    // de lo contrario no hay página para remover de la navStack lo que genera excepción.
    if (msgDeleted) {
      this.inboxService.deleteMessage(msgID);
      this.navCtrl.pop().then(() => { this.presentToast(toastMsg); });
    }

    console.log("[updateMessageStatus] - Code: " + codeID + " | MsgID: " + msgID);
  }

  /**
   * Muestra una alerta en el extremo inferior de la pantalla.
   * @param mensaje Mensaje a mostrar
   */
  private presentToast(mensaje) {
    let toast = this.toastCtrl.create({
      message: mensaje,
      duration: 3000,
      position: 'bottom'
    });
    toast.present();
  }
}