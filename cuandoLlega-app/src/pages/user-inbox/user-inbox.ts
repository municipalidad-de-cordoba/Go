import { Component, Injectable } from '@angular/core';
import { NavController, Events, MenuController } from 'ionic-angular';
import { UserInboxServiceProvider } from "../../providers/user-inbox-service/user-inbox-service";
import { UserMessage } from "../../app/globals";

import { Platform } from 'ionic-angular';
import { HomePage } from './../home/home';
import { UserInboxMsgPage } from "./user-inbox-msg";

@Component({
  selector: 'page-user-inbox',
  templateUrl: 'user-inbox.html',
})

/**
 * @description Representa la página con la lista de mensajes de un usuario.
 */
@Injectable()
export class UserInboxPage {
  /** @description Lista de mensajes del usuario */
  userMsgList: Array<UserMessage> = [];

  constructor(private navCtrl: NavController, platform:Platform, private inboxService: UserInboxServiceProvider,
              private events: Events, private menuCtrl: MenuController) {
    
    platform.registerBackButtonAction(() => { 
      if (this.menuCtrl.isOpen()) {
        this.menuCtrl.close();
      }
      else this.navCtrl.setRoot(HomePage); }, 1)
  }

  ionViewDidLoad() {
    //console.log('ionViewDidLoad UserInboxPage');
  }

  ionViewDidEnter() {
    // Obtenemos el listado de mensajes una vez que se haya cargado la página y la misma esté activa.
    this.userMsgList = this.inboxService.getUserMessages();
  }

  /**
   * Obtiene los detalles de un mensaje en particular
   * @param curMsg Mensaje del cual obtener los detalles
   */
  public openUserMsgDetail(curMsg:UserMessage) {
    this.navCtrl.push(UserInboxMsgPage, { item: curMsg });

    // Marcamos el mensaje como leido.
    if (curMsg.unread) {
      curMsg.unread = false;

      // Actualizamos la data de memoria interna
      this.inboxService.saveMessagesToStorage(this.userMsgList);

      // Publicamos el evento inbox:msgCount para que se actualice el contador de mensajes
      // en el menú.
      this.events.publish("inbox:msgCount");
    }
  }

  volverHome() {
    this.navCtrl.setRoot(HomePage);
  }

}
