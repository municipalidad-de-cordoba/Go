import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UserInboxPage } from './user-inbox';

@NgModule({
  declarations: [
    UserInboxPage,
  ],
  imports: [
    IonicPageModule.forChild(UserInboxPage),
  ],
})
export class UserInboxPageModule {}
