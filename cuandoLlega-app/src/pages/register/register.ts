import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { AuthServiceProvider } from './../../providers/auth-service/auth-service';
import { MessagesServiceProvider } from './../../providers/messages-service/messages-service';

import { HomePage } from './../home/home'
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { LoginPage } from './../login/login'
import { CacheProvider } from '../../providers/cache-service/cache-service';
import { CacheType } from '../../providers/cache-service/cache-enum';

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {

  responseData : any;
  userData = {"username":"", "password1":"", "password2":"", "name":"", "email":"", "genero":""};
  errors = {"name": false, "username": false, "email": false, "password1": false, "password2": false, "genero": false};
  errores ={"password1": "", "username": "", "email": ""};

  constructor(public navCtrl: NavController, public navParams: NavParams, public authService : AuthServiceProvider,
            private messagesService : MessagesServiceProvider, private statusBar:StatusBar, private cache:CacheProvider) {
  }

  ionViewDidLoad() {
    this.statusBar.styleDefault();
  }

  volverAWelcome():void {
    this.navCtrl.pop();
  }

  registerUser() {
    let registrar = true;

    if (this.userData.password1 != this.userData.password2) {
      alert("Las contraseñas no coinciden.");
      registrar = false;
    }

    //TODO: (Si es que se puede hacer) --> Si token existe, chequear que sea valido
    //TODO: Comprobar que los campos no esten vacios
    if (registrar) {
      this.authService.postData(this.userData, "registration").then((result) => {

        this.responseData = result;
        if(this.responseData.token){
          this.cache.setItemAsync(CacheType.USER_DATA, JSON.stringify(this.responseData));
          //this.navCtrl.push(HomePage);
          this.navCtrl.setRoot(HomePage);
        }
        else {
          let msg = "Su cuenta ha sido creada con éxito! Un mail llegará a su correo electrónico para activar la cuenta.";
          msg += "Si no encuentra el mail, por favor revise la carpeta de SPAM / Correo no deseado.";
          this.messagesService.presentToast(msg, "top", null, 10000);
          //this.navCtrl.push(LoginPage);
          this.navCtrl.setRoot(LoginPage);
        }
      }, (err) => {
        this.errores ={"password1": "", "username": "", "email": ""};
        let usuario = JSON.parse(err.error);

        if(usuario.username != null){this.errores.username = usuario.username;this.errors.username = true;}
        if(usuario.email != null){this.errores.email = usuario.email;this.errors.email = true;}
        if(usuario.password1 != null){this.errores.password1= usuario.password1;this.errors.password1 = true;this.errors.password2 = true;}

      });
    }

  }

}
