import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RedbusPage } from './redbus';

@NgModule({
  declarations: [
    RedbusPage,
  ],
  imports: [
    IonicPageModule.forChild(RedbusPage),
  ],
})
export class RedbusPageModule {}
