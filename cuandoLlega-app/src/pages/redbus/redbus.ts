import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController } from 'ionic-angular';
import { HomePage } from './../home/home';
import { FavoritosPage } from './../favoritos/favoritos';
import { TrackingPage } from '../tracking/tracking';
import { Platform } from 'ionic-angular';

/**
 * Generated class for the RedbusPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
 /*Mapa*/
 import {
   GoogleMaps,
   GoogleMap,
   GoogleMapOptions,
   MarkerIcon,
   GoogleMapsEvent,
 } from '@ionic-native/google-maps';
 import { ApiServiceProvider } from './../../providers/api-service/api-service';
 import xml2js from 'xml2js';

@IonicPage()
@Component({
  selector: 'page-redbus',
  templateUrl: 'redbus.html',
})
export class RedbusPage {

  /*coordenas mapa*/
  curLat = -31.4201;
  curLng = -64.1888;

  map3: GoogleMap;

  constructor(public navCtrl: NavController, public navParams: NavParams, private apiService:ApiServiceProvider,
              public platform: Platform, private menuCtrl: MenuController) {
    platform.registerBackButtonAction(() => {
        if (this.menuCtrl.isOpen()) {
            this.menuCtrl.close();
          }
          else this.navCtrl.setRoot(HomePage);
    },1);
  }

  ionViewDidLoad() {
    this.initMap();
    this.map3.getMyLocation().then(result =>  {
      this.map3.setMyLocationEnabled(true);
      this.curLat = result.latLng.lat;
      this.curLng = result.latLng.lng;
      this.map3.animateCamera({
          target: {lat: this.curLat, lng: this.curLng},
          zoom: 15,
          duration: 500
        }).then(() => {});
    });
  }

  initMap() {
    let mapOptions: GoogleMapOptions = {
      gestures: {
        scroll: true,
        tilt: false,
        zoom: true,
        rotate: true
      },
      preferences: {
        zoom: {
          minZoom: 14,
          maxZoom: 18
        }
      },

      styles: [
        {
            "featureType": "water",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#e9e9e9"
                },
                {
                    "lightness": 17
                }
            ]
        },
        {
            "featureType": "landscape",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#f5f5f5"
                },
                {
                    "lightness": 20
                }
            ]
        },
        {
            "featureType": "road.highway",
            "elementType": "geometry.fill",
            "stylers": [
                {
                    "color": "#ffffff"
                },
                {
                    "lightness": 17
                }
            ]
        },
        {
            "featureType": "road.highway",
            "elementType": "geometry.stroke",
            "stylers": [
                {
                    "color": "#ffffff"
                },
                {
                    "lightness": 29
                },
                {
                    "weight": 0.2
                }
            ]
        },
        {
            "featureType": "road.arterial",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#ffffff"
                },
                {
                    "lightness": 18
                }
            ]
        },
        {
            "featureType": "road.local",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#ffffff"
                },
                {
                    "lightness": 16
                }
            ]
        },
        {
            "featureType": "poi",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#f5f5f5"
                },
                {
                    "lightness": 21
                }
            ]
        },
        {
            "featureType": "poi.park",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#c0ecae"
                },
                {
                    "lightness": 21
                }
            ]
        },
        {
            "elementType": "labels.text.stroke",
            "stylers": [
                {
                    "visibility": "on"
                },
                {
                    "color": "#ffffff"
                },
                {
                    "lightness": 16
                }
            ]
        },
        {
            "elementType": "labels.text.fill",
            "stylers": [
                {
                    "saturation": 36
                },
                {
                    "color": "#333333"
                },
                {
                    "lightness": 40
                }
            ]
        },
        {
            "elementType": "labels.icon",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "transit",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#f2f2f2"
                },
                {
                    "lightness": 19
                }
            ]
        },
        {
            "featureType": "administrative",
            "elementType": "geometry.fill",
            "stylers": [
                {
                    "color": "#fefefe"
                },
                {
                    "lightness": 20
                }
            ]
        },
        {
            "featureType": "administrative",
            "elementType": "geometry.stroke",
            "stylers": [
                {
                    "color": "#fefefe"
                },
                {
                    "lightness": 17
                },
                {
                    "weight": 1.2
                }
            ]
        }
    ] ,

      camera: {
        target: {
          lat: this.curLat,
          lng: this.curLng
        },
        zoom: 15
      },

    };
    this.map3 = GoogleMaps.create('map3', mapOptions);
    //this.map.one(GoogleMapsEvent.MAP_READY).then(this.onMapReady.bind(this));

    // console.log('Mapa ONE ')
    // Wait the MAP_READY before using any methods.
    this.map3.one(GoogleMapsEvent.MAP_READY)
    .then(() => {
      // Now you can use all methods safely.
      this.map3.setMyLocationEnabled(true);
      let results : any;
      let res : any;
      let markerNodes :any;
      let paradaIcon: MarkerIcon = {
          url: './assets/imgs/expendio.png',
          size: {
            width: 40,
            height: 60
          }
      };
      let paradaTarjeta: MarkerIcon = {
          url: './assets/imgs/tarjeta-red-bus.png',
          size: {
            width: 40,
            height: 60
          }
      };
      this.apiService.getRedBus().subscribe(
        data => { results = data},
        err => {
            results = err.error.text;
            xml2js.parseString(results, { explicitArray: false }, (error, result) => {
               if (error) {
                 throw new Error(error);
               } else {
                 res = result;
                 markerNodes = res.markers.marker;
                for (let i = 0; i < markerNodes.length; i++) {
                  if(markerNodes[i].$.tipo == "pdv"){
                    this.map3.addMarker({
                                  position: {lat: markerNodes[i].$.lat, lng: markerNodes[i].$.lng},
                                  icon: paradaIcon,
                                  title: markerNodes[i].$.address,
                                  snippet: markerNodes[i].$.barrio + ' - (Boca Nro.'+ markerNodes[i].$.boca +')'
                                });
                    }
                  else{this.map3.addMarker({
                                position: {lat: markerNodes[i].$.lat, lng: markerNodes[i].$.lng},
                                icon: paradaTarjeta,
                                title: markerNodes[i].$.address,
                                snippet: markerNodes[i].$.barrio + ' - (Boca Nro.'+ markerNodes[i].$.boca +')'
                              });
                    }
                }
               }

             });
        });

      // this.geolocation.getCurrentPosition()
      //     .then(response => {
      //         this.curLat = response.coords.latitude;
      //         this.curLng = response.coords.longitude;
      //
      //         this.map.animateCamera({
      //             target: {lat: this.curLat, lng: this.curLng},
      //             zoom: 16.5,
      //             duration: 500
      //           }).then(() => {});
      //     })
      //     .catch(error =>{
      //       console.log(error);
      //     });

    })
    .catch(error =>{
      //console.log(error);
    });

  }
  pageHome(){
    this.navCtrl.setRoot(HomePage);
  }
  pageEspera(){
    this.navCtrl.setRoot(TrackingPage);
  }
  pageFavoritos(){
    this.navCtrl.setRoot(FavoritosPage);
  }

}
