import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform } from 'ionic-angular';

import { AuthServiceProvider } from './../../providers/auth-service/auth-service';

import { MessagesServiceProvider } from './../../providers/messages-service/messages-service';
import { TwitterConnect } from '@ionic-native/twitter-connect/ngx';
import { HomePage } from './../home/home';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { GooglePlus } from '@ionic-native/google-plus/ngx';

import { LoggerServiceProvider } from "./../../providers/logger-service/logger-service";
import { CacheProvider } from '../../providers/cache-service/cache-service';
import { CacheType } from '../../providers/cache-service/cache-enum';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  responseData : any;
  responseText : any;
  public userData = {"username":"", "password":""};

  constructor(public navCtrl: NavController, public navParams: NavParams,

    private authService : AuthServiceProvider, private messagesService : MessagesServiceProvider, 
    private twitter: TwitterConnect, private statusBar:StatusBar, private gplus:GooglePlus,
    private logs:LoggerServiceProvider, private platform:Platform, private cache:CacheProvider) {

    this.cache.getItemAsync(CacheType.USER_DATA).then((appData) => {
      if (appData) this.navCtrl.setRoot(HomePage);
      else gplus.logout();
    });
  }

  ionViewWillEnter() {
    this.statusBar.styleLightContent();
  }

  signInUser(){
    this.authService.postData(this.userData, "login").then((result) => {
      this.responseData = result;
      this.responseData = JSON.parse(this.responseData.data);
      if (this.responseData.key) {
        //this.navCtrl.push(HomePage);
        this.cache.setItemAsync(CacheType.USER_DATA, JSON.stringify(this.responseData));
        this.navCtrl.setRoot(HomePage);
      }
    }, (err) => {
      console.log(err);
      this.messagesService.presentToast("El usuario o contraseña son incorrectos.", "top",null,4000)
    });

  }
  volverAWelcome():void{
    this.navCtrl.pop();
  }

  twitterLogin(){
    this.twitter.login().then(res => {
      // console.log(res);
      if (res.token) {
        let twitterData = {
          "access_token": res.token,
          "token_secret": res.secret
        };
        // console.log("TWITTER DATA ACA: "+twitterData.access_token + ", "+ twitterData.token_secret+"\n");
        this.authService.postData(twitterData, "twitter").then((result) => {
          this.responseData = result;
          if (this.responseData.token) {
            this.cache.setItemAsync(CacheType.USER_DATA, JSON.stringify(this.responseData));
            this.navCtrl.setRoot(HomePage);
          }
        }, (err) => {
          console.log(err);
          this.messagesService.presentToast("Credenciales Erroneas", "top",null,3000)
        });

      }
    })
    .catch(err => console.error(err));

  }

  forgotPassword() {
    this.navCtrl.push("ForgotPasswordPage");
  }

  register():void {
    this.navCtrl.push("RegisterPage");
  }


  googleLogin(){
    this.logs.dbLog("[login.ts] - Funcion googleLogin");

    //Dejo esto para cuando haya que pasar a version web
    // if(this.platform.is('cordova')){
      this.nativeGoogleLogin();
    // }else{
    //   this.webGoogleLogin();
    // }
  }
  
  googleSilentLogin(){
    this.logs.dbLog("[login.ts] - Funcion googleSilentLogin");
    this.gplus.trySilentLogin(
      {
        'webClientId' : '921693927680-ivg0uhtacshj43ccd5cp55g31beulkp9.apps.googleusercontent.com',
        'offline': true,
        'scopes': 'profile email'
      }
    ).then(res => {
        this.logs.dbLog("[login.ts] - Entro en Silent login de google:");
        this.logs.dbLog(res);
      })
      .catch(err => {
        this.logs.dbErr("Error en silent login de Google");
        this.logs.dbErr(err);
      });
    }

nativeGoogleLogin() {

  this.logs.dbLog("[login.ts] - Funcion nativeGoogleLogin");
  this.responseText = "clicked";

  let strWebClientId = '921693927680-ucv9s0mar16lujn29hmb07vm0ukfs4n4.apps.googleusercontent.com'; 
  let origenStr = 'web';

  if(this.platform.is('ios')){
    strWebClientId = '921693927680-ivg0uhtacshj43ccd5cp55g31beulkp9.apps.googleusercontent.com';
    origenStr = 'android'; //Por configuraciones de google y el plugin.
  }

  this.gplus.login(
    {
      'webClientId' : strWebClientId,
      'offline': true,
      'scopes': 'profile email'
    }
  ).then(res => {

      this.logs.dbLog("[login.ts] - Entro en google");
      this.logs.dbLog("[gplus.login() res] - " + res);
      this.responseText = res.idToken;

      this.cache.getItemAsync(CacheType.DEVICE_ID).then((tmpDeviceID) => {
        let deviceID = JSON.parse(tmpDeviceID);

        if (res.userId) {
          // let googleData = {
          //   "google_id": res.userId,
          //   "nombre": res.displayName,
          //   "url_imagen": res.imageUrl,
          //   "email": res.email,
          //   "id_token": res.idToken
          // };
          let dataToSend = {
            "id_token": res.idToken,
            "device_id": deviceID,
            "origen": origenStr
          }
  
          this.logs.dbLog("[pre authService.googleLogin()]");
          this.authService.googleLogin(dataToSend).then((result) => {
            this.responseData = result;
            this.responseData = JSON.parse(this.responseData.data);
  
            this.logs.dbLog("[responseData]: " + this.responseData);
            
            if (this.responseData.token){
              this.logs.dbLog("[authService.googleLogin()] - Token correcto. Logueado con google.");
  
              this.responseData.key = this.responseData.token;
              
              this.cache.setItemAsync(CacheType.USER_DATA, JSON.stringify(this.responseData));
              this.navCtrl.setRoot(HomePage);
            }
          }, (err) => {
            this.logs.dbErr("[ERROR: authService.googleLogin()] - " + err);
            this.responseText = err.error;
            
            this.messagesService.presentToast("Error al intentar ingresar con Google", "top",null,3000)
          });
        }
      });
    })
    .catch(err => {
      this.logs.dbErr("ERROR en login de Google: ");
      this.logs.dbErr(err);
      this.messagesService.presentToast("Error al conectar con Google", "top",null,3000)
      this.responseText = err;

    });
  }
}

