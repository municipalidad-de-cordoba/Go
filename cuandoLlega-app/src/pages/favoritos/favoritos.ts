import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController } from 'ionic-angular';
import { HomePage } from './../home/home';
import { RecGeometry } from './../../app/globals';
import { AlertController } from 'ionic-angular';
import { TrackingPage } from './../tracking/tracking';
import { ApiServiceProvider } from './../../providers/api-service/api-service';
import { Platform } from 'ionic-angular';
import { CacheProvider } from '../../providers/cache-service/cache-service';
import { CacheType } from '../../providers/cache-service/cache-enum';

@IonicPage()
@Component({
  selector: 'page-favoritos',
  templateUrl: 'favoritos.html',
})
export class FavoritosPage {
  favorito:boolean = false;
  favoritos : any;
  tamano : number = 1 ;
  items = [];
  paramsParaSegPagEspera :any = {
    'parametros' :"",
    'colectivo':"",
    'descripcion':"",
    'recorrido':"",
    'parada':"",
    'geometry': RecGeometry
  };

  // #239_config-api
  private maxColectivosEspera :number = 6;

  constructor(public navCtrl: NavController, public navParams: NavParams, private apiService:ApiServiceProvider, private cache:CacheProvider,
    public alertCtrl: AlertController, platform: Platform, private menuCtrl: MenuController) {      
      platform.registerBackButtonAction(() => {
        if (this.menuCtrl.isOpen()) {
          this.menuCtrl.close();
        }
        else this.navCtrl.setRoot(HomePage);
      }, 1);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FavoritosPage');
  }

  ionViewWillEnter() {
    this.loadFavoritosFromStorage();

    // #239_config-api: cantidad maxima de colectivos.
    this.cache.getItemAsync(CacheType.CONFIG_MAX_BONDI_ESPERA).then((maxColectivos) => {
      if (maxColectivos != null) this.maxColectivosEspera = parseInt(maxColectivos, 10);
      console.log("[ionViewWillEnter] - Max Colect Espera: " + this.maxColectivosEspera);
    });
  }

  loadFavoritosFromStorage() {
    // Obtenemos la lista de favoritos desde memoria.
    this.cache.getItemAsync(CacheType.LISTA_FAVORITOS).then((tmpVal) => {
      let val = JSON.parse(tmpVal);
      if (val != null) {
        this.favoritos = val;
        this.tamano = this.favoritos.length;
        console.log("tam", this.tamano);
      }
      else {
        this.tamano = 0;
        console.log("esta vacio")} 
    });
  }

  volverAWelcome():void{
    this.navCtrl.setRoot(HomePage);
  }

  eliminarFavorito(favorito) {
    this.favoritos.splice(this.favoritos.indexOf(favorito), 1);
    this.tamano = this.favoritos.length;

    this.cache.getItemAsync(CacheType.LISTA_FAVORITOS).then((tmpList) => {
      let list = JSON.parse(tmpList);
      if (list != null) {
        this.cache.setItemAsync(CacheType.LISTA_FAVORITOS, JSON.stringify(this.favoritos));
        console.log("guardo",this.favoritos)
      }
    });
  }

  volverHome() {
    this.navCtrl.setRoot(HomePage);
  }

  fav_Ficha(fav){
    let resRecorridoParadas : any;

    // Obtenemos información de distancia-tiempo del colectivo.
    this.apiService.getTracking(fav[4],fav[3]).subscribe(
      data => { resRecorridoParadas = data},
      err => {
          console.error(err);
      },
      () => {
        this.paramsParaSegPagEspera.parametros = resRecorridoParadas;
        this.paramsParaSegPagEspera.descripcion = fav[1];
        this.paramsParaSegPagEspera.colectivo = fav[0];
        this.paramsParaSegPagEspera.recorrido = fav[4];
        this.paramsParaSegPagEspera.parada = fav[3];
        this.paramsParaSegPagEspera.geometry =  fav[5];

        this.cache.getItemAsync(CacheType.LISTA_FICHAS).then((tmpList) => {
          let list = JSON.parse(tmpList);
          if (list != null) {
            if (list.length >= this.maxColectivosEspera) {
              console.log(list.length);
              let alert = this.alertCtrl.create();
              alert.setTitle('Ya seleccionaste ' + this.maxColectivosEspera + ' paradas, si queres cambiar podes hacerlo desde espera.');
              alert.present();
              return;
            }
            else {
              let posicion = this.search(this.paramsParaSegPagEspera , list);
              if (posicion != -1) {
                list.splice(posicion, 1);
                list.unshift(this.paramsParaSegPagEspera);
              }
              else {
                list.unshift(this.paramsParaSegPagEspera);
              }
            }
          }
          else {
            list = [];
            list.push(this.paramsParaSegPagEspera); 
          }
          this.cache.setItemAsync(CacheType.LISTA_FICHAS, JSON.stringify(list));
          this.navCtrl.setRoot(TrackingPage);
        });
      }
    );
  }

  search(element, array) {
    for (var i in array) {
      if (element.recorrido == array[i].recorrido && array[i].parada == element.parada) return i;
    }
    return -1;
  }

  confirmDeleteFicha(ficha: any) {
    let alert = this.alertCtrl.create({
      title: 'Eliminar Favorito: ' + ficha[2],
      message: "¿Está seguro que desea eliminar el favorito seleccionado?",
      buttons: [
        {
          text: 'Aceptar',
          handler: () => { this.eliminarFavorito(ficha); }
        },
        {
          text: 'Cancelar',
          role: 'cancel',
          handler: () => { console.log('Cancel clicked'); }
        }
      ]
    });
    alert.present();
  }

  editarFavorito(ficha: any) {
    // Buscamos el favorito en la lista.
    this.cache.getItemAsync(CacheType.LISTA_FAVORITOS).then((tmpList) => {
      let list = JSON.parse(tmpList);
      let curIndex = this.getFavIndex(list, ficha);
      if (curIndex != -1) {
        let alert = this.alertCtrl.create({
          title: 'Editar Favorito',
          inputs: [
            {
              name: 'nombre',
              placeholder: 'Nombre del favorito'
            }
          ],
          buttons: [
            {
              text: 'Aceptar',
              handler: data => {
                if (data.nombre) {
                  list[curIndex][2] = data.nombre;
                  this.nuevoFavorito(list);
                }
                else return false;
              }
            },
            {
              text: 'Cancelar',
              role: 'cancel',
              handler: data => { console.log('Cancel clicked'); }
            }
          ]
        });
        alert.present();
      }
    });
  }

  nuevoFavorito(list) {
    this.cache.setItemAsync(CacheType.LISTA_FAVORITOS, JSON.stringify(list)).then(() => {
      this.loadFavoritosFromStorage();
    });
  }

  getFavIndex(list, curFav):number {
    let curIndex = -1;

    if (list) {
      for (let i = 0; i < list.length; i++) {
        if (list[i]) {
          if (list[i][2] == curFav[2] && list[i][3] == curFav[3] && list[i][4] == curFav[4]) curIndex = i;
        }
      }
    }

    return curIndex;
  }
}
