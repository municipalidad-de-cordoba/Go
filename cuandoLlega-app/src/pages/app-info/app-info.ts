import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController } from 'ionic-angular';
import { AppVersion } from '@ionic-native/app-version/ngx';
import { HomePage } from './../home/home';
import { ApiServiceProvider } from './../../providers/api-service/api-service';
import { Platform } from 'ionic-angular';
import { LoggerServiceProvider } from '../../providers/logger-service/logger-service';

/**
 * Generated class for the AppInfoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-app-info',
  templateUrl: 'app-info.html',
})
export class AppInfoPage {
  versionNumber : string = "0.1";
  items: any = []; 
  itemExpandHeight1: number = 150;
  itemExpandHeight2: number = 580;
  itemExpandHeight3: number = 440;
  textos : any = [];
  
  // #335_logs-sistema
  logItemList: any = [];
  showLogList: boolean = false;
  // --


  constructor(public navCtrl: NavController, public navParams: NavParams, private appVersion: AppVersion,
    private menuCtrl: MenuController, private apiService:ApiServiceProvider, platform: Platform,
    private loggerService: LoggerServiceProvider) {
    let infoApp : any;
    this.items = [
      {expanded: false},
      {expanded: false},
      {expanded: false},
      {expanded: false},
      {expanded: false},
      {expanded: false},
      {expanded: false},
      {expanded: false},
      {expanded: false}
    ];

    this.apiService.getInfoApp().subscribe(
      data =>  {infoApp = data},
      err => {
        console.error(err);
        //this.presentToast("Ha ocurrido un error");
      },
      () => {
        this.textos.push(infoApp.about_app);
        this.textos.push(infoApp.terminos_y_condiciones);
        this.textos.push(infoApp.politica_de_privacidad);
        console.log("info app: ",infoApp.politica_de_privacidad);
      }
    )
    
    platform.registerBackButtonAction(() => {
      if (this.menuCtrl.isOpen()) {
        this.menuCtrl.close();
      }
      else this.navCtrl.setRoot(HomePage);
    },1);
  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad AppInfoPage');

    this.appVersion.getVersionNumber().then(version => {
      this.versionNumber = version;
    });

    // #335_logs-sistema: Mostramos todos los logs
    // Obtenemos el div en el cual se mostrarán los logs.
    this.logItemList = this.loggerService.getLogsList();
    this.showLogList = this.loggerService.isDevModeEnabled();
  }

  volverAWelcome():void{
    this.navCtrl.setRoot(HomePage);
  }

  expandItem(item) {
      this.items.map((listItem) => {

          if(item == listItem){
              listItem.expanded = !listItem.expanded;
          } else {
              listItem.expanded = false;
          }

          return listItem;

      });

  }

  volverHome() {
    this.navCtrl.setRoot(HomePage);
  }

  // #335_logs-sistema: Activa el modo desarrollador según la cantidad de clicks que se hayan hecho sobre un botón.
  enableDeveloperMode() {
      this.loggerService.incrementDevButtonClickCount();
      this.showLogList = this.loggerService.isDevModeEnabled();
  }

}
