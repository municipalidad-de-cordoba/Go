import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AppInfoPage } from './app-info';
import { ExpandableComponent } from './../../components/expandable/expandable';

@NgModule({
  declarations: [
    AppInfoPage,
    ExpandableComponent
  ],
  imports: [
    IonicPageModule.forChild(AppInfoPage),
  ],
})
export class AppInfoPageModule {}