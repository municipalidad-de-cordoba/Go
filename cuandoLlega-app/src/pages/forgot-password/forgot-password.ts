import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { MessagesServiceProvider } from './../../providers/messages-service/messages-service';
import { AuthServiceProvider } from './../../providers/auth-service/auth-service';
import { StatusBar } from '@ionic-native/status-bar/ngx';


@IonicPage()
@Component({
  selector: 'page-forgot-password',
  templateUrl: 'forgot-password.html',
})
export class ForgotPasswordPage {

  responseData : any;
  userData = {"email":""};
  constructor(public navCtrl: NavController, public navParams: NavParams, private authService:AuthServiceProvider, public messagesService:MessagesServiceProvider, private statusBar:StatusBar) {
  }

  ionViewDidLoad() {
    this.statusBar.styleDefault();
  }

  volverAWelcome():void {
    this.navCtrl.pop();
  }

  resetPassword() {
    this.authService.postData(this.userData, "password/reset").then((result) => {
      this.responseData = result;
      let msg = "Hemos reestablecido su contraseña, un mail le llegará a su correo electrónico. ";
      msg += "Si no encuentra el mail, por favor revise la carpeta de SPAM / Correo no deseado.";
      this.messagesService.presentToast(msg, "top", null, 10000);
      
      this.navCtrl.pop();
    }, (err) => {
      this.messagesService.presentToast("Error. Intenta nuevamente.", "top", null, 5000)
    });
  }

}
