import { BusTrackerCardComponent } from './../../components/bus-tracker-card/bus-tracker-card';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TerminosYCondicionesPage } from './terminos-y-condiciones';

@NgModule({
  declarations: [
    TerminosYCondicionesPage,
    BusTrackerCardComponent
  ],
  imports: [
    IonicPageModule.forChild(TerminosYCondicionesPage),
  ],
})
export class TerminosYCondicionesPageModule {}
