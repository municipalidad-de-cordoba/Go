import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Platform } from 'ionic-angular';
import { HomePage } from './../home/home';
import { MenuController } from '@ionic/angular';

@IonicPage()
@Component({
  selector: 'page-terminos-y-condiciones',
  templateUrl: 'terminos-y-condiciones.html',
})
export class TerminosYCondicionesPage {


  constructor(public navCtrl: NavController, public navParams: NavParams, platform: Platform, private menuCtrl: MenuController) {
    platform.registerBackButtonAction(() => {
      if (this.menuCtrl.isOpen()) {
        this.menuCtrl.close();
      }
      else this.navCtrl.setRoot(HomePage);
    },1);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TerminosYCondicionesPage');
  }

  volverAWelcome():void{
    this.navCtrl.pop({animate: true, direction: 'forward'})
  }

}
