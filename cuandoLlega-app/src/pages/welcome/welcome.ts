import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

 
import { HomePage } from './../home/home';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { CacheProvider } from '../../providers/cache-service/cache-service';
import { CacheType } from '../../providers/cache-service/cache-enum';

@IonicPage()
@Component({
  selector: 'page-welcome',
  templateUrl: 'welcome.html',
})
export class WelcomePage {

  responseData : any;
  
  userData = {"username":"", "password":""};

  constructor(public navCtrl: NavController, public navParams: NavParams, private statusBar:StatusBar,
    private cache:CacheProvider) {
    
  }

  ionViewDidLoad() {
    // console.log('ionViewDidLoad LoginPage');
    this.statusBar.styleLightContent();

    this.cache.getItemAsync(CacheType.USER_DATA).then((tmpData) => {
      let data = JSON.parse(tmpData);
      if (data != null && data.user) {
        this.userData = data.user;
      }
      // console.log(data.token);
      if (data != null && data.token) {
        this.navCtrl.setRoot(HomePage);
      }
    });
  }
  

  forgotPassword(){
    this.navCtrl.push("ForgotPasswordPage");
  }

  abrirRegister():void{
    this.navCtrl.push("RegisterPage");
  }
   
  abrirLogin():void{
    this.navCtrl.push("LoginPage");
  }



}
