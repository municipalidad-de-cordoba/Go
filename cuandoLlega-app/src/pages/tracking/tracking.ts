import { Bus, Poste, SeguimientoDistanciaTiempo } from './../../app/globals';
import { Component, NgZone } from '@angular/core';
import { IonicPage, NavController, LoadingController, Loading, MenuController } from 'ionic-angular';
import { HomePage } from './../home/home';
import { FavoritosPage } from './../favoritos/favoritos';
import { AlertController } from 'ionic-angular';
import { Subscription, forkJoin } from 'rxjs';

import { ApiServiceProvider } from './../../providers/api-service/api-service';
import { ViewChild } from '@angular/core';
import { Slides } from 'ionic-angular';
import { Platform } from 'ionic-angular';

import { ITrackingResponseData, ITrackingResponseResultado } from '../../shared/model/traking-response-data.model';
import { IFicha } from '../../shared/model/ficha.model';

/* Mapa */
import {
  GoogleMap,
  Marker,
  MarkerIcon,
  GoogleMapsEvent,
  Polyline,
  HtmlInfoWindow
} from '@ionic-native/google-maps';

import { LoggerServiceProvider } from '../../providers/logger-service/logger-service';
import { CacheProvider } from '../../providers/cache-service/cache-service';
import { CacheType } from '../../providers/cache-service/cache-enum';
import { MapProvider } from '../../providers/map-service/map-service';
import { TTSProvider } from '../../providers/tts-service/tts-provider';

const DEFAULT_FRECUENCIA_MILLIS = 10000;

const ERROR_CODE_NO_HAY_BONDIS = 2;
const ERROR_CODE_NO_HAY_GPS = 3;

@IonicPage()
@Component({
  selector: 'page-tracking',
  templateUrl: 'tracking.html',
})
export class TrackingPage {
  @ViewChild('slides') slides: Slides;

  favorito:boolean = false;

  sDistanciaTiempo : SeguimientoDistanciaTiempo[] = [];

  toTrackList: SeguimientoDistanciaTiempo[] = [];

  dTiempo : IFicha[];
  colectivosEsperados: any;
  tamano : number = 0;

  /*coordenas mapa*/
  curLat = -31.4201;
  curLng = -64.1888;

  map: GoogleMap;
  postesMarkers : Poste [] = [];
  busMarkers : Bus [] = [];
  posteSelectMarker : Poste [] = [];
  posteIncioFin = [];

  polys:Polyline[] =[];

  currentIndex : number = 0;
   
  _loading: Loading;
  subscriptions: Subscription[] = [];
 
  private maxColectivosFavoritos: number;

  /** Listado de paradas obtenidas desde cache. */
  private listParadas = [];

  /** Mantiene una referencia a los eventos de click de colectivo. */
  private busDetailListener: Subscription[] = [];

  htmlInfoWindow: HtmlInfoWindow;

  public ttip_internoID;
  public ttip_tiempo;
  public ttip_distancia;
  public ttip_adaptado;
  public ttip_horaActualizacion;

  /** Indica la calidad de la señal del gps:
   *  0 = buena
   *  1 = mala
   *  2 = muy mala
   */
  gpsSignalType: number = 0;

  bus_initialTime: number = 100;
  bus_initialTime_lastID: number = -1;
  bus_arribando: boolean = false;
  bus_pasaronTodos: boolean = false;

  isViewLoading: boolean = true;

  isAppPaused: boolean = false;

  constructor(public navCtrl: NavController, public alertCtrl: AlertController, public loadingCtrl: LoadingController,
    private apiService: ApiServiceProvider, public platform: Platform, private ngZone: NgZone, 
    private logService: LoggerServiceProvider, private cache: CacheProvider, private mapProvider: MapProvider,
    private menuCtrl: MenuController, private tts:TTSProvider) {

    this.initializeView();
  }

  /**
   * Realiza las acciones correspondientes a la iniciación de la vista.
   */
  private initializeView() {
    // Suscripciones a evento de pausa y resumen.
    this.platform.ready().then(() => {
      
      // Registramos la acción del botón de "Back"
      this.platform.registerBackButtonAction(() => {
        if (this.menuCtrl.isOpen()) {
          this.menuCtrl.close();
        }
        else this.navCtrl.setRoot(HomePage);
      }, 1);

      this.subscriptions.push(this.subscribeToPause());
      this.subscriptions.push(this.subscribeToResume());
    });
  }

  /**
   * Eventos que se disparan cuando la aplicación vuelve de segundo plano.
   */
  private subscribeToResume() {
    return this.platform.resume.subscribe(() => {
      this.logService.dbLog("[] Resuming...");
      //this.initializeNewInterval();
      this.isAppPaused = false;
    });
  }

  /**
   * Eventos que se disparan cuando la aplicación se pone en segundo plano.
   */
  private subscribeToPause() {
    return this.platform.pause.subscribe(() => {
      this.tts.stop();
      this.isAppPaused = false;
      //this.logService.dbLog("[] Pausing...");
      //this.cache.getItemAsync(CacheType.CODIGO_INTERVAL).then(this.detenerIntervalSiExiste());
    });
  }

  /**
   * Muestra el tiempo y distancia faltante del colectivo seleccionado
   * para llegar a la parada seleccionada.
   */
  bucleDistanciaTiempo(): void {
    let fichasToDelete: IFicha[] = [];
    this.isViewLoading = true

    this.subscriptions.push(forkJoin(
      this.dTiempo.map(ficha => this.apiCallToResolveOnCompletePromise(ficha, fichasToDelete))
    ).subscribe(() => {
      if (fichasToDelete.length < 1) return;
      this.deleteFichas(fichasToDelete);
    }, console.error));
  }

  private apiCallErrorHandler(ficha: IFicha, dataError: number, fichasToDelete: IFicha[]) {
    if (dataError == null) return;

    this.dismissLoading();
    //this.deleteFicha(ficha);
    /*
      // Si el colectivo ya pasó, añadimos la ficha a la lista de eliminación.
      if (pasaronColectivosEsperados) fichasToDelete.push(ficha);
    */
    switch (dataError) {
      case ERROR_CODE_NO_HAY_BONDIS:
        break;

      case ERROR_CODE_NO_HAY_GPS:
        break;
    }
  }

  ttsCounter = 0;
  /**
   * Actualiza la posición y datos del colectivo según los datos recibidos desde 
   * el API.
   * @param ficha Ficha seleccionada. 
   * @param fichasToDelete Listado de fichas a eliminar debido a que ya no son necesarias trackearlas.
   */
  private apiCallToResolveOnCompletePromise(ficha: IFicha, fichasToDelete: IFicha[]) {
    return new Promise((resolve) => {
      // Obtenemos la información del colectivo desde el API.
      this.subscriptions.push(this.apiService.getTracking(ficha.recorrido, ficha.parada).subscribe(data => {
        if (this.isAppPaused === true) return;
        console.log(JSON.stringify(data));

        ficha.parametros = data;

        // Identificamos el código de error para realizar las acciones correspondientes.
        if (data[0].error !== 0) {
          this.logService.dbErr("[getTrackingData]: " + data[0].detalles);

          this.apiCallErrorHandler(ficha, data[0].error, fichasToDelete);
          this.isViewLoading = false;
          return;
        }

        ficha.changed = 6*(data[0].resultados.tiempo_minutos - ficha.tiempo_inicial);
        if (ficha.tiempo_anterior != data[0].resultados.tiempo_minutos) {
          ficha.tiempo_anterior = data[0].resultados.tiempo_minutos;
        }
        
        const esFichaVisible = ficha == this.dTiempo[this.currentIndex];
        if (esFichaVisible) {
          this.getColectivo(this.map, ficha.parametros);
          this.dismissLoading();

          this.verifyGpsSignal(ficha.parametros[0].resultados.hora_ultima_posicion);
          this.setBusInitialTime(ficha.parametros[0].resultados);
          this.TTS_Speak_BusPosition(data, ficha);
          this.showBusArrivingAlert(ficha, ficha.parametros[0].resultados);

          if (ficha.parametros[0].error == 2) this.removeBus();
        }
      }, err => {
        console.error(err);
        this.dismissLoading();
      }, () => { resolve(); this.isViewLoading = false; }));
    });
  }

  private TTS_Speak_BusPosition(data: ITrackingResponseData[], ficha: IFicha) {
    if (this.ttsCounter == 0 && this.bus_arribando === false && this.isAppPaused === false) {
      let curDate = new Date();
      let curTime = curDate.getTime();
      let gpsTimeArray = data[0].resultados.hora_ultima_posicion.split(":");
      let gpsHour = parseInt(gpsTimeArray[0], 10);
      let gpsMins = parseInt(gpsTimeArray[1], 10);
  
      let gpsTime = new Date(curDate.getFullYear(), (curDate.getMonth()), curDate.getDay(), gpsHour, gpsMins, 0).getTime();
      let tmpRes = Math.abs(curTime - gpsTime) / 1000;
      let minsPassed = Math.floor(tmpRes / 60) % 60;

      let recorridoTxt = ficha.descripcion.replace("B\xB0", "barrio");
      let adaptadoTxt = (ficha.parametros[0].resultados.adaptado == true) ?
        ",adaptado," : ",no adaptado,";
      let distTxt = " se encuentra aproximadamente a " + data[0].resultados.distancia_km + "kilómetros y arribará a su parada";
      distTxt += " aproximadamente dentro de " + data[0].resultados.tiempo_minutos + " minutos.";

      let gpsTxt = "Este colectivo actualizó su posición por última vez hace " + minsPassed + "minutos.";

      let msg = "El colectivo " + ficha.colectivo + adaptadoTxt + " con recorrido " + recorridoTxt + distTxt;

      this.tts.speak(msg).then(() => {
        this.tts.speak(gpsTxt).then(() => {
          msg = "Además hay otros " + this.cantidadBondisSinPasar(data) + " colectivos en camino hacia su parada.";
          this.tts.speak(msg);
        });
      });

      this.ttsCounter++;
    }
    else {
      this.ttsCounter++;
      if (this.ttsCounter >= 4) this.ttsCounter = 0;
    }
  }

  private cantidadBondisSinPasar(busData: ITrackingResponseData[]): Number {
    let count = 0;
    for (let i = 0; i < busData.length; i++) {
      if (busData[i] && busData[i].resultados.ya_paso == false) count++;
    }

    return count;
  }

  /**
   * Muestra el cartel de "Arribando" cuando el colectivo se encuentra cerca de la parada
   * seleccionada por el usuario.
   * @param busData Información del colectivo recibida desde el API
   */
  private showBusArrivingAlert(fichaData, busData:ITrackingResponseResultado) {
    //if (busData.distancia_km <= 1 && !busData.ya_paso) this.bus_arribando = true;

    if (busData.distancia_km <= 1 && !busData.ya_paso) {
      let msg = "El colectivo " + fichaData.colectivo + " Interno " +  busData.numero_interno + " está arribando.";
      
      this.tts.speak(msg);

      this.bus_arribando = true;
    }
    else this.bus_arribando = false;
  }

  /** Descarta la alerta de carga. */
  private dismissLoading() {
    if (!this._loading || !this._loading.instance) return;

    this._loading.dismiss();
    this._loading = null;
  }

  slideChanging() {
    console.log("SLIDE CHANGING");
    this.tts.stop().then(() => this.isViewLoading = true);
  }

  slideChangeTimeout;
  /**
   * Evento que se dispara cuando el slide activo ha cambiado.
   */
  slideChanged() {
    clearTimeout(this.slideChangeTimeout);

    this.slideChangeTimeout = setTimeout(() => {
      this.ttsCounter = 0;  // Reiniciamos el contador para que TTS comience a hablar tan pronto el slide cambie.

      this.currentIndex = this.slides.getActiveIndex();
      
      // Nos aseguramos que haya un tracking activo en el slide seleccionado.
      if (!this.dTiempo[this.currentIndex]) { return; }

      this.clearMap();

      this.getPostesR(this.map, this.dTiempo[this.currentIndex].recorrido, this.dTiempo[this.currentIndex].parada);
      this.drawRecorrido(this.map, this.dTiempo[this.currentIndex]);
      
      if (this.dTiempo[this.currentIndex].parametros[0].error == 2) return;
      this.getColectivo(this.map, this.dTiempo[this.currentIndex].parametros);
      this.verifyGpsSignal(this.dTiempo[this.currentIndex].parametros[0].resultados.hora_ultima_posicion);
      this.setBusInitialTime(this.dTiempo[this.currentIndex].parametros[0].resultados);
      this.showBusArrivingAlert(this.dTiempo[this.currentIndex], this.dTiempo[this.currentIndex].parametros[0].resultados);
      this.TTS_Speak_BusPosition(this.dTiempo[this.currentIndex].parametros, this.dTiempo[this.currentIndex]);
    }, 500);
  }

  /**
   * Establece el valor máximo utilizado como referencia para dibujar el progreso del reloj
   * @param busData Información de tracking del colectivo.
   */
  private setBusInitialTime(busData: ITrackingResponseResultado) {
    if (this.bus_initialTime_lastID !== busData.numero_interno) {
      this.bus_initialTime = busData.distancia_km;
      this.bus_initialTime_lastID = busData.numero_interno;
    }
  }

  /**
   * Limpia todos los marcadores del mapa.
   */
  clearMap() {
    this.removeBus();
    this.removePolys();
    this.removePosteInicioFin();
    this.removePostes();
    this.removePosteSelect();
  }

  ionViewWillLeave() {
    this.logService.dbLog("[Tracking.ts] - ionViewCanLeave");

    this.cache.getItemAsync(CacheType.CODIGO_INTERVAL).then(this.detenerIntervalSiExiste());
    this.cache.setItemAsync(CacheType.COLECTIVOS_ESPERADOS, JSON.stringify(this.colectivosEsperados));

    this.unsubscribe();
    this.unsubscribeBusMarker();

    // Silenciamos text-to-speech.
    this.tts.stop();
  }

  ionViewCanLeave() {
    if (this.isViewLoading === true) {
      let alert = this.alertCtrl.create({
        buttons: [
          {
            text: "Aceptar"
          }
        ],
        message: "Todavía estamos procesando su consulta anterior.",
      });
      alert.present();
      return false;
    }
    else return true;
  }

  ionViewDidEnter() {
    this.logService.dbLog("[TrackingPage] - ionViewDidEnter");

    this.initMap();
  }

  /**
   * Evento que se dispara al inicializar la vista.
   */
  ionViewDidLoad() {
    this.logService.dbLog("[Tracking] - ionViewDidLoad");
    
    this.cache.getItemAsync(CacheType.COLECTIVOS_ESPERADOS)
    .then((value) => {
      if (value) {
        console.log("colectivos esperados: ", value);
        this.colectivosEsperados = JSON.parse(value) || {};
      }
      else this.colectivosEsperados = {};
    })
    .then(() => this.cache.getItemAsync(CacheType.LISTA_FICHAS))
    .then((tmpFicha) => {

      if (!tmpFicha) return;
      
      let aliveFichas = [];
      let fichas = JSON.parse(tmpFicha);
      fichas.forEach(ficha => {
        if (ficha === undefined) {
          console.log("ficha == undefined");
          return;
        }

          aliveFichas.push(ficha);
      });

      this.dTiempo = aliveFichas;
      this.tamano = this.dTiempo.length;

      if (this.tamano <= 0) return;

      this.initializeNewInterval();

      const currentVisibleFicha = this.dTiempo[this.currentIndex];

      if (!currentVisibleFicha || currentVisibleFicha.recorrido == null) return;

      this.drawRecorrido(this.map, currentVisibleFicha);
      this.getPostesR(this.map, currentVisibleFicha.recorrido, currentVisibleFicha.parada);

      if (currentVisibleFicha.parametros[0].error !== 2) this.verifyGpsSignal(currentVisibleFicha.parametros[0].resultados.hora_ultima_posicion);
    }).then(() => this.dismissLoading());
    
    // #239_config-api: cantidad maxima de colectivos.
    this.cache.getAppConfig_byKeyAsync(CacheType.CONFIG_MAX_BONDI_ESPERA).then((maxColectivos) => {
      if (maxColectivos != null) this.maxColectivosFavoritos = parseInt(maxColectivos, 10);

      console.log("[STORAGE] - Max Colect Espera: " + this.maxColectivosFavoritos);
    });
  }

  /**
   * Verifica la calidad de la señal del colectivo usando como referencia
   * la hora en la que se recibió la última señal del GPS.
   * @param horaUltPosicion Hora de la última vez que se recibió la señal del GPS
   */
  private verifyGpsSignal(horaUltPosicion:string) {
    let curDate = new Date();
    let curTime = curDate.getTime();
    let gpsTimeArray = horaUltPosicion.split(":");
    let gpsHour = parseInt(gpsTimeArray[0], 10);
    let gpsMins = parseInt(gpsTimeArray[1], 10);

    let gpsTime = new Date(curDate.getFullYear(), (curDate.getMonth()), curDate.getDay(), gpsHour, gpsMins, 0).getTime();
    let tmpRes = Math.abs(curTime - gpsTime) / 1000;
    let minsPassed = Math.floor(tmpRes / 60) % 60;

    if (minsPassed >= 2 && minsPassed < 4) this.gpsSignalType = 1;
    else if (minsPassed >= 4) this.gpsSignalType = 2;
    else this.gpsSignalType = 0;
  }

  private initializeNewInterval() {
    this.logService.dbLog("[] Initializing new interval...");
    this.cache.getItemAsync(CacheType.CODIGO_INTERVAL)
      .then(this.detenerIntervalSiExiste())
      .then(() => this.presentLoadingIfAnyFicha())
      .then(() => this.bucleDistanciaTiempo())
      .then(this.lanzarNuevoIntervalYGuardarCodigo())
      .catch(console.error);
  }

  private presentLoadingIfAnyFicha() {
    if (!this.dTiempo || this.dTiempo.length == 0) {
      return;
    }
 
    if (!this._loading) this.getLoading().present();
  }

  /** Detiene un intervalo de solicitud en caso de que el mismo exista. */
  private detenerIntervalSiExiste() {
    return (codigoInterval) => {
      if (!codigoInterval) return; 

      clearInterval(codigoInterval);
      this.logService.dbLog("Deteniendo Interval #: " + codigoInterval); 
    }
  }

  private lanzarNuevoIntervalYGuardarCodigo() {
    return () => {
      const intervalNumber = setInterval(() => {
        this.ngZone.run(() => {
          this.bucleDistanciaTiempo();
        });
      }, DEFAULT_FRECUENCIA_MILLIS);

      this.logService.dbLog("[Nuevo Intervalo] #:" + intervalNumber);
      this.cache.setItemAsync(CacheType.CODIGO_INTERVAL, JSON.stringify(intervalNumber));
    };
  }

  /**
   * Prepara el cartel de carga.
   */
  private getLoading() : Loading {
    this._loading = this._loading || this.loadingCtrl.create({
      content: 'Actualizando datos...',
      enableBackdropDismiss: true,
      dismissOnPageChange: true
    });

    return this._loading;
  }

  /**
   * Inicializa el mapa de Google.
   */
  initMap() {
    this.map = this.mapProvider.getInstance("map2");
    this.htmlInfoWindow = new HtmlInfoWindow();

    this.isViewLoading = false;

    this.map.getMyLocation().then(result =>  {
      this.curLat = result.latLng.lat;
      this.curLng = result.latLng.lng;

      this.map.setPadding(30, 10, 0, 0);
      this.map.setMyLocationEnabled(true);
      this.map.setMyLocationButtonEnabled(true);
    }).catch(error => {
      this.logService.dbErr("Error al obtener ubicación: " + JSON.stringify(error));

      this.map.setPadding(0);
      this.map.setMyLocationEnabled(false);
      this.map.setMyLocationButtonEnabled(false);
    });
  }

  getPostesR(map:GoogleMap, rec, poste) {
    let paradaSelectIcon: MarkerIcon = {
      url: './assets/imgs/image.png',
      size: {
        width: 30,
        height: 30
      }
    };
    let resultPoste;

    console.log("entro al getPostesR ", rec);
    
    // Nos aseguramos de limpiar el mapa antes de obtener nueva información.
    this.clearMap();

    // Utilizamos forkJoin para agrupar todas las solicitudes que se realicen al servidor
    // y quedarnos sólo con la última respuesta recibida.
    this.subscriptions.push(this.apiService.getPosteSelect(poste).subscribe(
      data => { resultPoste = data; },
      err => {
          console.error("Error", err);
          this.isViewLoading = false;
      },
      () => {
        this.removePosteSelect();
        map.addMarker({
          position: {lat: resultPoste.results.features[0].geometry.coordinates[1], lng: resultPoste.results.features[0].geometry.coordinates[0]},
          icon: paradaSelectIcon
        }).then((marker: Marker) => {
          let auxPoste = new Poste(resultPoste.results.features[0].id, resultPoste.results.features[0].properties.id_externo, resultPoste.results.features[0].properties.descripcion, resultPoste.results.features[0].properties.sentido, resultPoste.results.features[0].properties.calle_principal, resultPoste.results.features[0].properties.calle_secundaria, marker)
          if (this.posteSelectMarker.indexOf(auxPoste)==-1) {
            this.posteSelectMarker.push(auxPoste);
          }

          if (this.curLat  != resultPoste.results.features[0].geometry.coordinates[1] && this.curLng != resultPoste.results.features[0].geometry.coordinates[0]){
            this.curLat = resultPoste.results.features[0].geometry.coordinates[1];
            this.curLng = resultPoste.results.features[0].geometry.coordinates[0];
            map.animateCamera({
              target: {lat: this.curLat, lng: this.curLng},
              // zoom: 14,
              duration: 800
            }).then(() => this.isViewLoading = false);
          }
        }).catch(error => {
          console.error("Error al añadir marcador de poste seleccionado: ", error);
          this.isViewLoading = false;
        });
    }));
  }

  /**
   * Busca información de un poste basado en el ID del recorrido
   * @param postesData Listado de todos los postes disponibles en memoria.
   * @param recID ID del recorrido del cual seleccionar sus postes.
   */
  getPostesDataByRecID(postesList: any, recID: number) {
    let data = null;
    if (postesList == null) return data;

    for (let i = 0; i < postesList.length; i++) {
      let curPoste = postesList[i];

      if (curPoste.RecID == recID) {
        data = curPoste;
        break;
      }
    }

    return data;
  }

  /**
   * Establece los postes correspondientes a un recorrido seleccionado. 
   * @param map Instancia de Google Maps
   */
  getPostesMarker(map:GoogleMap) {
    if (this.postesMarkers.length > 0) {
      let filteredPostes : any = [];

      for (let i = 0; i <= this.postesMarkers.length; i++) {
        if (this.postesMarkers[i] == null) continue;
        let p = this.postesMarkers[i];

        if (p.marker.getPosition().lat < map.getVisibleRegion().southwest.lat || p.marker.getPosition().lat > map.getVisibleRegion().northeast.lat
        ||  p.marker.getPosition().lng < map.getVisibleRegion().southwest.lng || p.marker.getPosition().lng > map.getVisibleRegion().northeast.lng) {
            p.marker.remove();
        }
        else filteredPostes.push(p);
      }
      this.postesMarkers = filteredPostes;
    }
  }

  /**
   * Obtiene información desde el API de los postes de un recorrido seleccionado.
   * @param rec Recorrido seleccionado.
   * @param boundStr Boundaries
   */
  getPostesData(recID: any, boundStr: string, resultPoste) {
    let results : any;

    console.log("Solicitando Postes al servidor...");

    this.subscriptions.push(this.apiService.getPostesRecorrido(recID, boundStr).subscribe(
      data => { results = data},
      err => {
          console.error("Error", err);
      },
      () => {
        this.setPostesData(results, resultPoste);

        this.addParadasToCacheList(recID, JSON.stringify(results));
  
        this.cache.setItemAsync(CacheType.POSTES_RECORRIDO, JSON.stringify(this.listParadas));
    }));
  }

  setPostesData(results, resultPoste) {
    let paradaIcon: MarkerIcon = {
      url: './assets/imgs/map-marker-point.png',
      size: {
        width: 30,
        height: 30
      }
    };

    results.results.features.forEach(p => {
      if (p.geometry.coordinates[1] != resultPoste.results.features[0].geometry.coordinates[1] && 
          p.geometry.coordinates[0] != resultPoste.results.features[0].geometry.coordinates[0]) {
        this.map.addMarker({
            position: {lat: p.geometry.coordinates[1], lng: p.geometry.coordinates[0]},
            icon: paradaIcon
        }).then((marker: Marker) => {
          let auxPoste = new Poste(p.id, p.properties.id_externo, p.properties.descripcion, p.properties.sentido, p.properties.calle_principal, p.properties.calle_secundaria, marker)
          if (this.postesMarkers.indexOf(auxPoste) == -1) {
              this.postesMarkers.push(auxPoste);
          }
        }).catch(error => console.error("Error al añadir marcador PostesData: ", error));
      }
    });
  }

  drawRecorrido(map:GoogleMap, rec) {
    this.removePosteInicioFin();
    let paradaInicio: MarkerIcon = {
      url: './assets/imgs/inicio.png',
      size: {
        width: 60,
        height: 60
      }
    };
    let paradaFin: MarkerIcon = {
      url: './assets/imgs/fin.png',
      size: {
        width: 60,
        height: 60
      }
    };

    map.addMarker({
      position: {lat: rec.geometry.coordinates[0].lat, lng: rec.geometry.coordinates[0].lng},
      icon: paradaInicio
    }).then((marker: Marker) =>{
      let auxPoste = marker;
      if(this.posteIncioFin.indexOf(auxPoste)==-1){
        this.posteIncioFin.push(auxPoste);
      }
    }).catch(error => console.error("Error al añadir marcador InicioLinea: ", error));

    map.addMarker({
        position: {lat: rec.geometry.coordinates[rec.geometry.coordinates.length - 1].lat, lng: rec.geometry.coordinates[rec.geometry.coordinates.length - 1].lng},
        icon: paradaFin
    }).then((marker: Marker) => {
      let auxPoste = marker;
      if(this.posteIncioFin.indexOf(auxPoste)==-1){
          this.posteIncioFin.push(auxPoste);
      }
    }).catch(error => console.error("Error al añadir marcador FinLinea: ", error));

    map.addPolyline({
      points: rec.geometry.coordinates,
      'color': '#55acee',//this.getRandomColor(),
      'width': 4,
      'geodesic': true
      }).then((polyline: Polyline) => {
        if(this.polys.indexOf(polyline)==-1){
          this.polys.push(polyline);
        }
    });
  }

  getColectivo(map:GoogleMap, bus: ITrackingResponseData[]) {
    this.removeBus();

    let pasaronTodos = true;

    for (let i = 0; i < bus.length; i++) {
      if (bus[i] == null || bus[i].error == 2) continue;
      let datos = bus[i];
      let bondiIcon: MarkerIcon = {
        url: datos.resultados.ya_paso ? './assets/imgs/230x230-final_r.png' : './assets/imgs/230x230-final_g.png',
        size: {
          width: 35,
          height: 35
        }
      };

      map.addMarker({
        position: {lat: datos.resultados.latitud, lng: datos.resultados.longitud},
        icon: bondiIcon
      }).then((marker: Marker) => {
        let auxBus = new Bus(datos.id, marker);
        
        if (this.busMarkers.indexOf(auxBus) == -1) this.busMarkers.push(auxBus);

        this.busDetailListener.push(marker.on(GoogleMapsEvent.MARKER_CLICK).subscribe(() => {
          this.showUnitDetails(datos, marker);
        }));
      });

      if (bus[i].resultados.ya_paso === false) pasaronTodos = false;
    }

    this.bus_pasaronTodos = pasaronTodos;
  }

  /**
   * Muestra los detalles del colectivo seleccionado.
   */
  showUnitDetails(infoBondi, curMarker: Marker) {
    let parsedBondi = infoBondi["resultados"];
    let frame: HTMLElement = document.createElement("div");
    let gpsLastTime = parsedBondi.hora_ultima_posicion + "hs";

    // Para que angular detecte los cambios a la variable es necesario
    // que las variables se actualicen dentro de Angular Zone.
    this.ngZone.run(() => {
      let txt_tiempo = !parsedBondi.ya_paso ? parsedBondi.tiempo_minutos + " MIN" : "Ya pasó";
      let txt_dist = !parsedBondi.ya_paso ? parsedBondi.distancia_km + " KM" : "Ya pasó";

      this.ttip_internoID = "Int. " + parsedBondi.numero_interno;
      this.ttip_tiempo = txt_tiempo;
      this.ttip_distancia = txt_dist;
      this.ttip_adaptado = parsedBondi.adaptado === true ? " Si" : " No";
      this.ttip_horaActualizacion = gpsLastTime;
    });

    frame.innerHTML = document.getElementsByClassName("busInfoTooltip")[0].innerHTML;

    this.htmlInfoWindow.setContent(frame);
    this.htmlInfoWindow.open(curMarker);
  }

  removePosteInicioFin(){
    if(this.posteIncioFin.length > 0){
        this.posteIncioFin.forEach(p =>{
            p.remove();
        });
        this.posteIncioFin = [];
    }
  }
  
  removePostes() {
    for (let i = 0; i < this.postesMarkers.length; i++) {
      if (this.postesMarkers[i] != null) this.postesMarkers[i].marker.remove();
    }

    this.postesMarkers = [];
  }

  removePosteSelect() {
    for (let i = 0; i < this.posteSelectMarker.length; i++) {
      if (this.posteSelectMarker[i] != null) {
        this.posteSelectMarker[i].marker.remove();
      }
    }

    this.posteSelectMarker = [];
  }

  removeBus() {
    /*if (this.busMarkers.length > 0) {
      this.busMarkers.forEach(p =>{
          p.marker.remove();
      });
      this.busMarkers = [];
      this.unsubscribeBusMarker();
    }*/

    for (let i = 0; i < this.busMarkers.length; i++) {
      if (this.busMarkers[i]) {
        this.busMarkers[i].marker.remove();
      }
    }

    this.busMarkers = [];
    this.unsubscribeBusMarker();
  }

  removePolys(){
    for (let i = 0; i < this.polys.length; i++) {
      if (this.polys[i]) {
        this.polys[i].remove();
      }
    }

    this.polys = [];
  }

  /** Vuelve a la página principal. */
  volverHome() {
    console.log("Volviendo home...");

    this.navCtrl.setRoot(HomePage);
  }

  pageFavoritos() {
    this.navCtrl.setRoot(FavoritosPage);
  }

  confirmDeleteFicha(ficha: IFicha) {
    let alert = this.alertCtrl.create({
      title: 'Eliminar Ficha',
      message: "¿Está seguro que desea eliminar la ficha?",
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          handler: () => { console.log('Cancel clicked'); }
        },
        {
          text: 'Aceptar',
          handler: () => { this.deleteFicha(ficha); }
        }
      ]
    });
    alert.present();
  }

  /**
   * Elimina una ficha al hacer click sobre el botón de la papelera en la vista de fichas.
   * @param ficha Ficha a eliminar
   */
  deleteFicha(ficha: IFicha) {
    this.dTiempo.splice(this.dTiempo.indexOf(ficha), 1);
    delete this.colectivosEsperados[ficha.recorrido + '-' + ficha.parada];

    if (this.dTiempo.length <= 0) {
      this.clearMap();
      this.isViewLoading = false;
    }

    this.cache.setItemAsync(CacheType.COLECTIVOS_ESPERADOS, JSON.stringify(this.colectivosEsperados))
      .then(() => this.cache.getItemAsync(CacheType.LISTA_FICHAS))
      .then((tmpList) => {
        let list = JSON.parse(tmpList);
        if (list != null) {
          this.cache.setItemAsync(CacheType.LISTA_FICHAS, JSON.stringify(this.dTiempo));

          this.slides.update();
          if (this.slides.getActiveIndex() > 0) {
            this.slides.slideTo(this.slides.getPreviousIndex());
          }
          else this.slideChanged();
        }
    });
  }

  deleteFichas(fichas: IFicha[]) {
    fichas.forEach(ficha => {
      this.dTiempo.splice(this.dTiempo.indexOf(ficha), 1);
      delete this.colectivosEsperados[ficha.recorrido + '-' + ficha.parada];
    });

    if (this.dTiempo.length <= 0) {
      this.clearMap();
      this.isViewLoading = false;
    }

    this.cache.setItemAsync(CacheType.COLECTIVOS_ESPERADOS, JSON.stringify(this.colectivosEsperados))
    .then(() => this.cache.getItemAsync(CacheType.LISTA_FICHAS))
    .then((tmpFichas) => {
      if (!tmpFichas) return;

      this.cache.setItemAsync(CacheType.LISTA_FICHAS, JSON.stringify(this.dTiempo));
      
      this.slides.update();
      if (this.slides.getActiveIndex() > 0) {
        this.slides.slideTo(this.slides.getPreviousIndex());
      }
      else {
        this.slideChanged();
      //   .then(() => this.navCtrl.setRoot(this.navCtrl.getActive().component));
      }
    });
  }

  nuevoFavorito(numero, descripcion, nombre, parada, recorrido, geometry) {
    console.log("ENTRO EN FAV");
    this.cache.getItemAsync(CacheType.LISTA_FAVORITOS).then((tmpList) => {
      let list = JSON.parse(tmpList);
      if (list != null) {
        if (list.length < this.maxColectivosFavoritos) {
          let guardar = [];
          guardar.push(numero, descripcion, nombre,parada,recorrido,geometry)
          list.unshift(guardar);
          this.cache.setItemAsync(CacheType.LISTA_FAVORITOS, JSON.stringify(list));
          this.navCtrl.setRoot(FavoritosPage);
        }
        else {
          console.log(list.length);
          let alert = this.alertCtrl.create();
          alert.setTitle('Tenés más de ' + this.maxColectivosFavoritos + ' favoritos, elimina alguno.');
          alert.present();
          return;
        }
      }
      else {
        let list = [];
        let guardar = [];
        guardar.push(numero, descripcion, nombre,parada,recorrido,geometry)
        list.push(guardar);
        this.cache.setItemAsync(CacheType.LISTA_FAVORITOS, JSON.stringify(list));
        this.navCtrl.setRoot(FavoritosPage);
      }
    });
  }

  editarNombre(ficha) {
    console.log(ficha);
    this.cache.getItemAsync(CacheType.LISTA_FAVORITOS).then((tmpList) => {
      let list = JSON.parse(tmpList);
      if (this.existsFavorito(list, ficha.colectivo, ficha.descripcion, ficha.parada, ficha.recorrido)) {
        let alert = this.alertCtrl.create({
            title: 'Este favorito ya existe',
        });
        alert.present();
      }
      else {
        let alert = this.alertCtrl.create({
          title: 'Nuevo Favorito',
          inputs: [
              {
              name: 'nombre',
              placeholder: 'Nombre del favorito'
              }
          ],
          buttons: [
              {
              text: 'Cancelar',
              role: 'cancel',
              handler: data => {
                  console.log('Cancel clicked');
              }
              },
              {
              text: 'Agregar',
              handler: data => {
                  if (data.nombre) {
                      this.nuevoFavorito(ficha.colectivo,ficha.descripcion,data.nombre,ficha.parada,ficha.recorrido,ficha.geometry)
                  } else return false;
              }
            }
          ]
        });
        alert.present();
      }
    });
  }

  existsFavorito(list, numero, descripcion, parada, recorrido):boolean {
    let exists = false;
    if (list != null) {
      list.forEach(fav => {
        if (fav[0] == numero && fav[1] == descripcion && fav[3] == parada && fav[4]  == recorrido) {
          exists = true;
        }
      });
    }
    return exists;
  }

  getMinZoom() {
    return this.cache.getItemAsync(CacheType.CONFIG_MIN_ZOOM).then((tmpData) => {
      let dataRes = 14; 
      if (tmpData) {
        let data = JSON.parse(tmpData);
        dataRes = data;
        return dataRes;
      }
      return dataRes;
    });
  }

    /**
   * Añade una parada al listado de cache.
   * @param recID ID del recorrido al cual pertenece la parada.
   * @param value Datos del recorrido a almacenar.
   */
  addParadasToCacheList(recID, value: string) {
    if (!this.existsParadaOnCache(recID)) {
      this.listParadas.push({
        RecID: recID,
        Value: value
      });
    }
  }

  /**
   * Verifica si una parada ya fue añadida al cache.
   * @param recID ID del recorrido a buscar.
   */
  existsParadaOnCache(recID) {
    for (let i = 0; i < this.listParadas.length; i++) {
      if (this.listParadas[i].RecID == recID) return true;
    }
    return false;
  }

  /**
   * Limpia las suscripciones generales de la vista.
   */
  unsubscribe(): any {
    console.log("(**) Limpiando suscripciones en Tracking.ts");

    /*this.subscriptions.forEach((subscription) => {
      subscription.unsubscribe();
      console.log(subscription);
    });*/
    for (let i = 0; i < this.subscriptions.length; i++) {
      if (this.subscriptions[i]) {
        this.subscriptions[i].unsubscribe();
        this.subscriptions[i] = undefined;
        this.subscriptions.splice(i, 1);
      }
    }
  }

  /**
   * Limpia las suscripciones de los marcadores del bondi.
   */
  unsubscribeBusMarker() {
    console.log("(**) Limpiando suscripciones de colectivo...");

    for (let i = 0; i < this.busDetailListener.length; i++) {
      if (this.busDetailListener[i] != null) {
        this.busDetailListener[i].unsubscribe();
        this.busDetailListener[i] = undefined;
        this.busDetailListener.splice(i, 1);
      }
    }
  }
}
