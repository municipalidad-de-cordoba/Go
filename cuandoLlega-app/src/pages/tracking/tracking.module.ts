import { BusTrackerCardComponent } from './../../components/bus-tracker-card/bus-tracker-card';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TrackingPage } from './tracking';

@NgModule({
  declarations: [
    TrackingPage,
    BusTrackerCardComponent
  ],
  imports: [
    IonicPageModule.forChild(TrackingPage),
    // BrowserModule,
    // IonicModule.forRoot(App),
    // IonicStorageModule.forRoot()
  ],
})
export class TrackingPageModule {}
