
export interface ITrackingResponseResultado {
  numero_interno?: number,
  longitud?: number,
  latitud?: number,
  piso_bajo?: boolean,
  adaptado?: boolean,
  articulado?: boolean,
  distancia_km?: number,
  tiempo_minutos?: number,
  ya_paso?: boolean,
  hora_ultima_posicion?: string
}

export class TrackingResponseResultado {
  constructor(
    public numero_interno?: number,
    public longitud?: number,
    public latitud?: number,
    public piso_bajo?: boolean,
    public adaptado?: boolean,
    public articulado?: boolean,
    public distancia_km?: number,
    public tiempo_minutos?: number,
    public ya_paso?: boolean,
    public hora_ultima_posicion?: string
  ) { }
}

export interface ITrackingResponseData {
    id?: string,
    error?: number,
    detalles?: string,
    resultados?: ITrackingResponseResultado
}

export class TrackingResponseData {
  constructor(
    public id?: string,
    public error?: number,
    public detalles?: string,
    public resultados?: ITrackingResponseResultado
  ) { }
}
