import { ITrackingResponseData } from "./traking-response-data.model";

export interface IFicha {
  descripcion?: string,
  colectivo?: string,
  recorrido?: string,
  parada?: string,
  parametros?: ITrackingResponseData[],
  tiempo_anterior?:number,
  tiempo_inicial?:number,
  changed?:number
}

export class Ficha {
  constructor(
    public descripcion?: string,
    public colectivo?: string,
    public recorrido?: string,
    public parada?: string,
    public parametros?: ITrackingResponseData[],
    public tiempo_anterior?:number,
    public tiempo_inicial?:number,
    public changed?:number
  ) { }
}
