
export interface IDatosInternosResponseData {
  registrados?: number,
  en_punta_de_lineas?: number,
}

export class DatosInternosResponseData {
  constructor(
    public registrados?: number,
    public en_punta_de_lineas?: number,
  ) { }
}
