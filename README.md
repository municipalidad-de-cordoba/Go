# Cuando llega
App JS de información sobre el arribo del colectivo que espero a la parada donde estoy

## Código
Copiar los archivos config-example.xml y package-example.json para crear los archivos config.xml y package.json.  
Estos archivos incluyen las credenciales necesarias para el funcionamiento de la app.  

Para compilar el sistema puede ver el documento [compilar-android.md](compilar-android.md).  