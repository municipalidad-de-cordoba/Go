# Compilar para Google Play de Android

En primer lugar es necesario dar de alta la aplicación en Google Play y luego comenzar el proceso de compilación.

## Instalaciones requeridas

### Node JS y gradle

```
sudo apt-get install -y nodejs npm
sudo apt-get install gradle
```

### Cordova, Ionic y dependencias de este proyecto

```
cd cuandoLlega-app
sudo npm install -g cordova
sudo npm install -g ionic
# Instalar las dependencias
npm install
```

### Ejecutar el script build.sh
```
Desde el directorio cuandoLlega-app ejecutamos ./build.sh
```

### Los siguientes pasos descriptos abajo son los que hace el script

### Variables de entorno

Revisar en el SDK las versones instaladas (Android Studio puede servir).  
Preparar variables de entorno.  

```
export ANDROID_HOME=$HOME/Android/Sdk
export PATH=$PATH:$ANDROID_HOME/tools/bin
export PATH=$PATH:$ANDROID_HOME/platform-tools
export JAVA_HOME=/usr/lib/jvm/java-1.8.0-openjdk-amd64/
export JAVA_CMD=/usr/lib/jvm/java-1.8.0-openjdk-amd64/bin/java
```

### Compilar
Antes de compilar es recomendable siempre actualizar la versión.
Esto se hace en los archivos _config.xml_ y _package.json_.  

```
# Ejecutar en emulador local (asegurarse que funcione)
ionic cordova run android
# Mejor emulador con logs (-c) y live reload (-l)
ionic cordova run android -lc

# Compilar prueba local
ionic cordova build android
```

### Firmar

Hacer firma (solo la primera vez).  
Guardar la clave y el archivo keystore en un lugar seguro (fuera de repo público).  

```
keytool -genkey -v -keystore go.keystore -alias Go -keyalg RSA -keysize 2048 -validity 10000

# Warning -> El almacén de claves JKS utiliza un formato propietario. Se recomienda migrar a PKCS12
keytool -importkeystore -srckeystore go.keystore -destkeystore go.keystore -deststoretype pkcs12

# Compilar release
ionic cordova build --release android
```

Firmar

```
jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore cuando-llega.keystore platforms/android/app/build/outputs/apk/release/app-release-unsigned.apk Go

rm platforms/android/app/build/outputs/apk/release/Go.apk

zipalign -v 4 platforms/android/app/build/outputs/apk/release/app-release-unsigned.apk platforms/android/app/build/outputs/apk/release/Go.apk
```